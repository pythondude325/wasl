const std = @import("std");

pub fn GenInterner(comptime key_size: type) type {
    return struct {
        const Self = @This();

        pub const Key = struct {
            _key: key_size,

            pub fn into_usize(self: Key) usize {
                return @intCast(usize, self._key);
            }
            
            fn from_usize(int: usize) Key {
                return Key { ._key = @intCast(key_size, int) };
            }
        };

        allocator: *std.mem.Allocator,
        // This might be an array of slice indicies later
        storage: std.ArrayList([]const u8),
        map: std.StringHashMap(Key),

        pub fn init(allocator: *std.mem.Allocator) Self {
            return .{
                .allocator = allocator,
                .storage = std.ArrayList([]const u8).init(allocator),
                .map = std.StringHashMap(Key).init(allocator),
            };
        }

        pub fn deinit(self: *Self) void {
            self.map.deinit();
            for (self.storage.items) |str| {
                self.allocator.free(str);
            }
            self.storage.deinit();
        }

        pub fn resolve(self: *const Self, key: Key) []const u8 {
            return self.storage.items[key.into_usize()];
        }

        pub fn get(self: *const Self, str: []const u8) ?Key {
            return self.map.get(str);
        }

        pub fn get_or_intern(self: *Self, str: []const u8) !Key {
            if(self.get(str)) |key| {
                // String is already interned

                return key;
            } else {
                // String is not already interned, make a new entry

                const key = Key.from_usize(self.storage.items.len);
                
                const owned_str = try self.allocator.dupe(u8, str);
                // If inserting into storage or map should fail, we need to
                // deallocate this instead of leaking it
                errdefer self.allocator.free(owned_str);

                try self.storage.append(owned_str);
                try self.map.put(str, key);
                return key;
            }
        }

        fn debug(self: *const Self) void {
            for(self.storage.items) |item, index| {
                std.debug.print("s[{d}]: '{s}'\n", .{index, item});
            }
            var it = self.map.iterator();
            while(it.next()) |kv| {
                std.debug.print("m['{s}']: {d}\n", .{kv.key, kv.value});
            }
        }
    };
}

test "iterner" {
    var interner = GenInterner(u32).init(std.testing.allocator);
    defer interner.deinit();

    std.debug.print("sizeof key: {}\n", .{@sizeOf(@TypeOf(interner).Key)});

    const k1 = try interner.get_or_intern("hi");
    std.debug.print("k: {d}\n", .{k1});

    const k2 = try interner.get_or_intern("bye");
    std.debug.print("k: {d}\n", .{k2});

    const k3 = try interner.get_or_intern("hi");
    std.debug.print("k: {d}\n", .{k3});

    interner.debug();

    std.debug.print("v: {d}\n", .{interner.get("hello")});

    std.debug.print("w: {s}\n", .{interner.resolve(k1)});
    std.debug.print("w: {s}\n", .{interner.resolve(k2)});
}
