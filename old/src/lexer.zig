const std = @import("std");
const interner = @import("./interner.zig");
const StaticList = @import("./StaticList.zig").StaticList;

const Interner = interner.GenInterner(u32);

fn isHexDigit(char: u8) bool {
    switch(char) {
        '0'...'9', 'a'...'f', 'A'...'F' => return true,
        else => return false,
    }
}

fn hexDigitToValue(char: u8) u4 {
    switch(char) {
        '0' => return 0x0,
        '1' => return 0x1,
        '2' => return 0x2,
        '3' => return 0x3,
        '4' => return 0x4,
        '5' => return 0x5,
        '6' => return 0x6,
        '7' => return 0x7,
        '8' => return 0x8,
        '9' => return 0x9,
        'a', 'A' => return 0xa,
        'b', 'B' => return 0xb,
        'c', 'C' => return 0xc,
        'd', 'D' => return 0xd,
        'e', 'E' => return 0xe,
        'f', 'F' => return 0xf,
        else => unreachable,
    }
}

const RawToken = struct {
    slice: []const u8,
    kind: enum {
        OpenParen,
        CloseParen,
        String,
        Symbol,
        Int,
    },

    // Strings as specified by https://webassembly.github.io/spec/core/text/values.html#strings
    fn read_string(input: []const u8) ?[]const u8 {
        if(input[0] != '"'){
            return null;
        }

        var pos: usize = 1;
        stringchar: while(pos < input.len) : (pos += 1) {
            switch(input[pos]){
                '\x00'...'\x20' => return null,
                '"' => {
                    return input[0..pos+1];
                },
                '\\' => {
                    pos += 1;
                    if(pos >= input.len){
                        return null;
                    }
                    switch(input[pos]){
                        't', 'n', 'r', '\'', '"' => continue,
                        'u' => {
                            unreachable; // unicode escapes not implemented yet.
                        },
                        '0'...'9', 'a'...'f', 'A'...'F' => {
                            pos += 1;
                            if(pos >= input.len){
                                return null;
                            }
                            if(isHexDigit(input[pos])){
                                continue;
                            } else {
                                return null;
                            }
                        },
                        else => return null,
                    }
                },
                '\x7f' => return null,
                else => continue :stringchar,
            }
        }
        return null;
    }

    fn read_integer(input: []const u8) ?[]const u8 {
        var digit_start: usize = 0;
        var pos: usize = 0;
        while(pos < input.len) : (pos += 1) {
            switch(input[pos]){
                ' ', '\t', '\n', ',', '(', ')', ';' => {
                    break;
                },
                '-', '+' => {
                    if(pos != 0){
                        return null;
                    }
                    digit_start = pos + 1;
                },
                '0'...'9', '_' => {
                    continue;
                },
                else => {
                    return null;
                }
            }
        }
        if(pos > digit_start){
            return input[0..pos];
        } else {
            return null;
        }
    }

    const Error = error {
        InvalidToken,
    };

    fn read(input: []const u8) Error!?RawToken {
        var pos: usize = 0;
        while(pos < input.len) : (pos += 1) {
            switch(input[pos]){
                '(' => return RawToken {
                    .slice = input[pos..pos+1],
                    .kind = .OpenParen,
                },
                ')' => return RawToken {
                    .slice = input[pos..pos+1],
                    .kind = .CloseParen,
                },
                ' ', '\t', '\n', ',' => {
                    continue;
                },
                // Skips comments
                ';' => {
                    while(pos < input.len and input[pos] != '\n'){
                        pos += 1; 
                    }
                    continue;
                },
                '"' => {
                    if(RawToken.read_string(input[pos..])) |slice| {
                        return RawToken {
                            .slice = slice,
                            .kind = .String,
                        };
                    } else {
                        return Error.InvalidToken;
                    }
                },
                else => {
                    if(RawToken.read_integer(input[pos..])) |slice| {
                        return RawToken {
                            .slice = slice,
                            .kind = .Int,
                        };
                    } else {
                        const end = end_block: {
                            var end: usize = pos + 1;
                            while(end < input.len) : (end += 1) {
                                switch(input[end]){
                                    ' ', '\t', '\n', ',', '(', ')', ';' => {
                                        break :end_block end;
                                    },
                                    else => {}
                                }
                            }
                            break :end_block end;
                        };

                        return RawToken {
                            .slice = input[pos..end],
                            .kind = .Symbol,
                        };
                    }
                }
            }
        }

        return null;
    }
};

pub const Token = union(enum) {
    OpenParen,
    CloseParen,
    Integer: i64,
    Symbol: Interner.Key,
    String: Interner.Key,
};

pub const Lexer = struct {

    source: []const u8,
    symbol_interner: *Interner,

    pub fn new(source: []const u8, symbol_interner: *Interner) Lexer {
        return Lexer { .source = source, .symbol_interner = symbol_interner };
    }

    pub fn next(self: *Lexer) !?Token {
        if(try RawToken.read(self.source)) |token| {
            const source_start = @ptrToInt(&self.source[0]);
            const token_start = @ptrToInt(&token.slice[0]);
            const token_offset = token_start - source_start;
            self.source = self.source[token_offset + token.slice.len..];

            // std.debug.print("'{s}'\n", .{token.slice});

            switch(token.kind) {
                .OpenParen => { return Token.OpenParen; },
                .CloseParen => { return Token.CloseParen; },
                .Int => {
                    var positive = true;
                    var value: i64 = 0;
                    for(token.slice) |c| {
                        switch(c){
                            '+' => { positive = true; },
                            '-' => { positive = false; },
                            '0'...'9' => {
                                const digit_value = c - '0';
                                value = value * 10 + @as(i64, digit_value);
                            },
                            else => { unreachable; },
                        }
                    }
                    if(positive){
                        return Token { .Integer = value };
                    } else {
                        return Token { .Integer = -value };
                    }
                },
                .Symbol => {
                    const key = try self.symbol_interner.get_or_intern(token.slice);
                    return Token { .Symbol = key };
                },
                .String => {
                    var buffer: [1000]u8 = undefined;
                    var fba = std.heap.FixedBufferAllocator.init(&buffer);
                    var allocator = &fba.allocator;
                    var string_builder = std.ArrayList(u8).init(allocator);
                    // Shouldn't need this
                    // defer string_builder.deinit();

                    const source = token.slice[1..token.slice.len-1];
                    var pos: usize = 0;
                    while(pos < source.len) : (pos += 1) {
                        switch(source[pos]){
                            '\x00'...'\x20', '"', '\x7f' => unreachable,
                            '\\' => {
                                switch(source[pos+1]){
                                    't' => {
                                        try string_builder.append('\t');
                                    },
                                    'r' => {
                                        try string_builder.append('\r');
                                    },
                                    'n' => {
                                        try string_builder.append('\n');
                                    },
                                    '\\' => {
                                        try string_builder.append('\\');
                                    },
                                    '"' => {
                                        try string_builder.append('"');
                                    },
                                    'u' => { unreachable; },
                                    '0'...'9','a'...'f','A'...'F' => {
                                        const n = hexDigitToValue(source[pos+1]);
                                        const m = hexDigitToValue(source[pos+2]);
                                        const value = 16 * @as(u8, n) + @as(u8, m);
                                        try string_builder.append(value);
                                        pos += 1;
                                    },  
                                    else => { unreachable; },
                                }
                                pos += 1;
                            },
                            else => {
                                try string_builder.append(source[pos]);
                            },
                        }
                    }

                    const key = try self.symbol_interner.get_or_intern(string_builder.items);
                    return Token { .String = key };
                }
            }

            return token;
        } else {
            return null;
        }
    }
};

test "tokens" {

    const program =
        \\(i32.mul
        \\ (i32.load 3)
        \\ (i32.const -4))
    ;

    var interner = Interner.init(std.testing.allocator);
    defer interner.deinit();
    var lexer = Lexer.new(program, &interner);

    while(try lexer.next()) |token| {
        std.debug.print("{s}\n", .{token});
    }
}
