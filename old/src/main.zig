const std = @import("std");
const interner = @import("./interner.zig");
const lexer = @import("./lexer.zig");

const Interner = interner.GenInterner(u32);

const SExpr = union(enum) {
    int: i64,
    identifier: Interner.Key,
    string: Interner.Key,
    pair: *Cell,
    nil,

    pub fn write(
        self: SExpr,
        writer: anytype,
        symbol_interner: ?*const Interner,
    ) @TypeOf(writer).Error!void {
        try self._write(writer, symbol_interner, false);
    }

    pub fn _write(
        self: SExpr,
        writer: anytype,
        symbol_interner: ?*const Interner,
        in_list: bool,
    ) @TypeOf(writer).Error!void {
        switch(self){
            .int => |value| try writer.print("{d}", .{value}),
            .identifier => |key| {
                if(symbol_interner) |int| {
                    try writer.print("{s}", .{int.resolve(key)});
                } else {
                    try writer.print("<symbol:{d}>", .{key.into_usize()});
                }
            },
            .string => |key| {
                if(symbol_interner) |int| {
                    // TODO: need to escape string characters
                    try writer.print("\"{s}\"", .{int.resolve(key)});
                } else {
                    try writer.print("<string:{d}>", .{key.into_usize()});
                }
            },
            .pair => |cell| {
                if(!in_list){
                    try writer.print("(", .{});
                } else {
                    try writer.print(" ", .{});
                }
                try cell.value._write(writer, symbol_interner, false);
                try cell.next._write(writer, symbol_interner, true);
            },
            .nil => {
                if(in_list){
                    try writer.print(")", .{});
                } else {
                    try writer.print("()", .{});
                }
            },
        }
    }

    pub fn format(
        self: SExpr,
        comptime fmt: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        try self.write(writer, null);
    }
};

const Cell = struct {
    value: SExpr = .nil,
    next: SExpr = .nil,
};

fn readValueSExpr(token: lexer.Token) SExpr {
    switch(token){
        .OpenParen, .CloseParen => unreachable,
        .Integer => |integer| {
            return SExpr { .int = integer };
        },
        .Symbol => |key| {
            return SExpr { .identifier = key };
        },
        .String => |key| {
            return SExpr { .string = key };
        },
    }
}

fn read(program_lexer: *lexer.Lexer, allocator: *std.mem.Allocator, cell_arena: *std.heap.ArenaAllocator) !SExpr {
    if(try program_lexer.next()) |token| {
        switch(token){
            .OpenParen => {
                var value: SExpr = .nil;
                var next_node_stack = std.ArrayList(*SExpr).init(allocator);
                defer next_node_stack.deinit();
                try next_node_stack.append(&value);

                while(try program_lexer.next()) |next_token| {
                    const next_node = next_node_stack.popOrNull() orelse { break; };

                    switch(next_token){
                        .CloseParen => {
                            next_node.* = SExpr.nil;
                        },
                        else => {
                            const cell = try cell_arena.allocator.create(Cell);
                            next_node.* = SExpr { .pair = cell };
                            try next_node_stack.append(&cell.*.next);

                            switch(next_token){
                                .CloseParen => unreachable,
                                .OpenParen => {
                                    try next_node_stack.append(&cell.*.value);
                                },
                                .Integer, .Symbol, .String => {
                                    cell.*.value = readValueSExpr(next_token);
                                },
                            }
                        },
                    }
                }

                if(next_node_stack.items.len != 0){
                    return error.NodesOnStack;
                } else {
                    return value;
                }
            },
            .CloseParen => {
                return error.UnexpectedParenthesis;
            },
            .Integer, .Symbol, .String => {
                return readValueSExpr(token);
            }
        }
    } else {
        return error.UnexpectedEnd;
    }
}

const program =
    \\ (module
    \\  (func $addTwo (export "addTwo") (param i32 i32) (result i32)
    \\    local.get 0
    \\    local.get 1
    \\    i32.add))
;

pub fn main() anyerror!void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = &gpa.allocator;
    defer {
        const leaked = gpa.deinit();
        if(leaked){
            std.debug.print("warning: leaked memory\n", .{});
        }
    }
    var symbol_interner = Interner.init(allocator);
    defer symbol_interner.deinit();
    var cell_arena = std.heap.ArenaAllocator.init(allocator);
    defer cell_arena.deinit();

    var program_lexer = lexer.Lexer.new(program, &symbol_interner);

    const expr = try read(&program_lexer, allocator, &cell_arena);

    var stdout = std.io.getStdOut().writer();

    try expr.write(stdout, &symbol_interner);
    try stdout.print("\n", .{});
}
