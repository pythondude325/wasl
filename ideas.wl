
should pointers have const-ness? there is no read-only memory like in x86

zero-sized types? if we have them, they have to get all compiled away. presumably in memory layout


local/stack values:
may be memory values or table values

memory values:
numerics: i32, u32, i8, etc
pointer: pointer to a memory value, represented by i32 (on wasm32)
struct: composite of memory values given names
array: a static number of homogeneous-typed memory values

table values:
funcrefs: reference to a typed function
externrefs: opaque values from the runtime

int * int -> int * int





let main = 
    let (d, r) = divrem a b



: slice { type: T -- type } { usize: len T many-pointer: ptr } ;
: vector { type: T -- type } { usize: len T slice: items } ;

: slice-len { T => T slice: s -- usize } ;


{i32 -- } "print_number" #print_number import-function

{i32 -- } funcref #temptable def-table 

: dup { T => T: v -- T T } v v ;
: over { T U => T: a U: b -- T U T } a b a ;
: swap { T U => T: a U: b -- U T } b a ;


: swap { a b } b a ;
: dup 0 pick ;
: over 1 pick ;

: pick { T: item N vararg: params comptimeint: N -- N vararg T } item params item ;



: add-two { i32 i32 -- i32 } + ;

#add-two { i32 i32 -- i32 } "add_two" export-function

i32 pointer



3 'a let


f(x) - f(x + h) / h

(def-func slope (f x h)
    (/ (- (f x) (f (+ x h)))
        h))


end


const M: memory;


fn swap<A, B>(a: A, b: B): B A {
    =(a b)
}


{ S I F =>  }





(deffun (T) (vector-capacity (v (vector T)))
    (slice-len (vector-items v)))

(deffun (T) (vector-ptr (v (vector T)))
    (slice-ptr (vector-items v)))

; Could we automatically box local values into memory values?

i32 pointer
file pointer

i32 3 array 

(pointer i32)
(pointer file)
(array i32)
(array 3 i32)

(tuple i32 i32 (pointer i32))

(deftype (vec3 t) (array 3 t))

(pointer )


(struct reader (i32 i32 i32))

(deffun (x ))







new idea: combination of lisp and forth

all expressions return multiple values
variables can only hold one value though (unless tuples somehow)
function call syntax will eat as many values it needs

so you can do `(values (values 1 2) 3 (values 4 5))` and it will give `1 2 3 4 5`
(values is just the variadic identity function)

interchangeable square brackets and round brackets (and curly brackets too i guess)

Idenifiers all share the same namespace and use lexical scoping
Identifiers can bind to functions (top level), local variables, or control labels

let syntax:
(let ([q r (divrem 13 5)])
    ; q is 2, r is 3
    
    
    )


casting:
(as <type> <expr>) 


(defun factorial (n) (i32 -- i32)
    (control-loop l ([a 1] [n n])
        (if (= n 1)
            a
            (l (* l n) (- n 1)))))

(control-loop l ([a 1] [n n])
    (br-if l (= n 1) (* l n) (- n 1))
    a)

;; (defun fma (a b c) (i32 i32 i32 -- i32)
;;     (+ (* a b) c))

(def-func fma ((a i32) (b i32) (c i32)) (i32)
    (+ (* a b) c))

;; (defun divrem (d a) (i32 i32 -- i32 i32)
;;     (values (/ d a) (% d a)))

(def-func divrem ((d i32) (a i32)) (i32 i32)
    (values (/ d a) (% d a)))

(defun foo () (-- i32)
    (+ (divrem 12 5) 3)) ; will return 2 + 2 + 3 = 7


Tough example from Eutro:
(define (app-comp f g x)
  (f (g x)))

(def-func-generic ((A) (B) (C)) app-comp
    ((f (func (B) (C)))
     (g (func (A) (B)))
     (x (rest C)))
    (f (g x)))

(defun-generic app-comp (f g x)
    ([a (variadic)]
     [b (variadic)]
     [c (variadic)])
    ((fn b -- c) (fn a -- b) a -- c)

    (f (g x)))


(defun-generic apply
    (f x ...)
    ([a (variadic)]
     [b (variadic)])
    ((fn a -- b) a -- b)

    (f x))




(apply + 1 2 3)


(const x 3)



(const x i32 3)

(const x i32 (+ 3 2))

(def-func add_i32 (a i32 b i32) (i32)
    (%inline-asm (i32 i32) (i32) ()
        (i32.add) a b)

()



(def-func add ((a i32) (b i32)) (i32)
    (; function body goes here ;))

(def-func-gen (T) + ((a T) (b T)) (T)
    (; body goes here ;))

(:: add (for T (func (T T) T)))
(def-func add (a b)
    )


(trait (Add rhs)
    (type Output))



(:: )

(for (γ α (... β))
    (func
        ((func (α (... β β)) (γ)) (listof α) (... β (listof β)))
        ((listof γ))))


(: + 





(def-func (+ x . xs) (for (T <: Add) (func (T (... T)) (T))))
    (if (= 0 (tuple-len xs))
        x
        (let (x2 xs (tuple-split xs))
            (+ (add x x2) xs))))

(+ 1 2 3)

(def-func +/3 ((x i32) (xs (tuple i32 i32))) (i32)
    (let (x2 xs (tuple-split xs))
            (+ (add x x2) xs)))

(for ((...+ β) (...+ α))
    (func
        ((func ((... α α)) ((... β β))) (... α (listof α)))
        ((... β (listof β)))))

(for (γ (... δ) α (... β))
    (func
        ((func (a (... β β)) (γ (... δ δ))) (listof α) (... β (listof β)))
        ((listof γ) (... δ (listof δ)))
        ))



(for)

(: map
    (for (γ α β ...)
        (func ((func (α β ...) (γ)) (listof α) (listof β) ...) (listof γ))))

(: map
    (for (γ ...) (for (α β ...))
        (func
            ((func (α β ...) (γ ...)) (listof α) (listof β) ...)
            ((listof γ) ...))))

(def-func)

(for T (func (T T) (T)))


(:: i32 3)




(∨ )


;; ∀r. Add r ∧ Mul r

(∀ r (∧ (Add r) (Mul r))



(def-trait Add ((R))
    (type Output)
    (func add ((self Self) (rhs R)) ((Add-Output Self))))

(impl-trait (Add i32) i32
    (def-type Output i32)
    (def-func add ((self i32) (rhs i32)) (i32)
        (%inline-asm (i32 i32) (i32) ()
            (i32.add) self rhs)))

(impl-trait (Add i64) i64
    (def-type Output i64)
    (def-fun)
)





(def-func add (a b) (func (i32 i32) (i32))
    (%inline-asm (i32 i32) (i32) ()
        (i32.add)
        a b))

(loop for (a 0)
	(if (< a 10)
		(begin
			(body)
			(for (+ a 1)))
		(values)))


(local $var-43 i32)

i32.const
(loop $label-42 (i32)
	local.set $var-43
	local.get $var-43
	i32.const 10
	32.leq
	(if $label-44
		(block (body)
			local.get $var-43
			i32.const 1
			i32.add
			br $label-42)
		(block ())))








(def-struct )


(def)