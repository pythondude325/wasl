functions:
	largest unit of code

	has a type signature

	takes in some number of values on the stack and returns some number of values

	Parameters get mapped onto locals

	extra locals may also be created


control structures:

	block:
		break jumps to end of block

	loop:
		break jumps to beggining of loop

	if:
		takes one i32 value
		if non-zero execute then
		if zero execute else

		break jumps to end of if

	br: jump to control break
	br_if: take one i32 value, if non-zero, jump to control break





: f (i32 i32 -- i32 i32)






i32 *mut

i32 *const



i32 *many

i32


i32 *const
i32 *mut

i32 array


u8 3 ntuple

{ i32 i32 } tuple






: point (T: type -- type)
	{ x: T y: T } struct ;

: slice (T: type -- type)
	{ data: T many-pointer len: usize } struct ;




: pointf32 f32 point ;
: pointi32 i32 point ;





(i32 i32 -- i32 i32 i32)






5 loop(l)

0 > br_if(l)




: 1+ 1 + ;











