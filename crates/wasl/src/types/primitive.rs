use crate::compiler::interner::Interner;
use crate::sexpr::Syntax;

use super::Type;

pub const PREINTERN_SYMBOLS: &[&str] = &[
    PRIMITIVE_I32_SYMBOL,
    PRIMITIVE_I64_SYMBOL,
    PRIMITIVE_F32_SYMBOL,
    PRIMITIVE_F64_SYMBOL,
    PRIMITIVE_V128_SYMBOL,
    PRIMITIVE_FUNCREF_SYMBOL,
    PRIMITIVE_EXTERNREF_SYMBOL,
    PRIMITIVE_FUNC_SYMBOL,
    PRIMITIVE_PARAM_SYMBOL,
    PRIMITIVE_RESULT_SYMBOL,
];

pub const PRIMITIVE_I32_SYMBOL: &str = "i32";
pub const PRIMITIVE_I64_SYMBOL: &str = "i64";
pub const PRIMITIVE_F32_SYMBOL: &str = "f32";
pub const PRIMITIVE_F64_SYMBOL: &str = "f64";
pub const PRIMITIVE_V128_SYMBOL: &str = "v128";
pub const PRIMITIVE_FUNCREF_SYMBOL: &str = "funcref";
pub const PRIMITIVE_EXTERNREF_SYMBOL: &str = "externref";
pub const PRIMITIVE_FUNC_SYMBOL: &str = "func";
pub const PRIMITIVE_PARAM_SYMBOL: &str = "param";
pub const PRIMITIVE_RESULT_SYMBOL: &str = "result";

#[derive(Clone, Copy, Debug)]
pub enum ValueType {
    I32,
    I64,
    F32,
    F64,
    V128,
    FuncRef,
    ExternRef,
}

impl ValueType {
    // fn as_str(&self) -> &'static str {
    //     match self {
    //         ValueType::I32 => "i32",
    //         ValueType::I64 => "i64",
    //         ValueType::F32 => "f32",
    //         ValueType::F64 => "f64",
    //         ValueType::V128 => "v128",
    //         ValueType::FuncRef => "funcref",
    //         ValueType::ExternRef => "externref",
    //     }
    // }

    pub fn into_syntax(&self, interner: &Interner) -> Syntax {
        match self {
            ValueType::I32 => Syntax::build_id(interner.static_symbol(PRIMITIVE_I32_SYMBOL)),
            ValueType::I64 => Syntax::build_id(interner.static_symbol(PRIMITIVE_I64_SYMBOL)),
            ValueType::F32 => Syntax::build_id(interner.static_symbol(PRIMITIVE_F32_SYMBOL)),
            ValueType::F64 => Syntax::build_id(interner.static_symbol(PRIMITIVE_F64_SYMBOL)),
            ValueType::V128 => Syntax::build_id(interner.static_symbol(PRIMITIVE_V128_SYMBOL)),
            ValueType::FuncRef => {
                Syntax::build_id(interner.static_symbol(PRIMITIVE_FUNCREF_SYMBOL))
            }
            ValueType::ExternRef => {
                Syntax::build_id(interner.static_symbol(PRIMITIVE_EXTERNREF_SYMBOL))
            }
        }
    }
}

#[derive(Clone, Debug)]
pub struct ResultType {
    types: Vec<ValueType>,
}

impl ResultType {
    pub const UNIT: ResultType = ResultType::unit();

    pub const fn unit() -> ResultType {
        ResultType { types: Vec::new() }
    }

    fn new<const N: usize>(arr: [ValueType; N]) -> ResultType {
        arr.into_iter().collect()
    }

    pub fn into_syntax(&self, interner: &Interner) -> Syntax {
        Syntax::build_list()
            .append(self.types.iter().map(|t| t.into_syntax(interner)))
            .build()
    }
}

impl From<ValueType> for ResultType {
    fn from(t: ValueType) -> Self {
        ResultType::new([t])
    }
}

impl<const N: usize> From<[ResultType; N]> for ResultType {
    fn from(rs: [ResultType; N]) -> Self {
        rs.into_iter().flat_map(|r| r.types).collect()
    }
}

impl<const N: usize> From<[ValueType; N]> for ResultType {
    fn from(ts: [ValueType; N]) -> Self {
        ts.into_iter().collect()
    }
}

impl AsRef<[ValueType]> for ResultType {
    fn as_ref(&self) -> &[ValueType] {
        self.types.as_ref()
    }
}

impl FromIterator<ValueType> for ResultType {
    fn from_iter<T: IntoIterator<Item = ValueType>>(iter: T) -> Self {
        ResultType {
            types: iter.into_iter().collect(),
        }
    }
}

impl IntoIterator for ResultType {
    type Item = ValueType;
    type IntoIter = std::vec::IntoIter<Self::Item>;
    fn into_iter(self) -> Self::IntoIter {
        self.types.into_iter()
    }
}

impl<'a> IntoIterator for &'a ResultType {
    type Item = &'a ValueType;
    type IntoIter = std::slice::Iter<'a, ValueType>;
    fn into_iter(self) -> Self::IntoIter {
        self.types.iter()
    }
}

#[derive(Clone, Debug)]
pub struct PrimitiveFunctionType {
    pub param: ResultType,
    pub result: ResultType,
}

impl PrimitiveFunctionType {
    pub fn from_funcref(func_ref: &super::FunctionReferenceType) -> PrimitiveFunctionType {
        PrimitiveFunctionType {
            param: func_ref.param.as_primitive(),
            result: func_ref.result.as_primitive(),
        }
    }

    pub fn into_syntax(&self, interner: &Interner) -> Syntax {
        Syntax::build_list()
            .sym(interner.static_symbol(PRIMITIVE_FUNC_SYMBOL))
            .syntax(
                Syntax::build_list()
                    .sym(interner.static_symbol(PRIMITIVE_PARAM_SYMBOL))
                    .append(self.param.into_syntax(interner))
                    .build(),
            )
            .syntax(
                Syntax::build_list()
                    .sym(interner.static_symbol(PRIMITIVE_RESULT_SYMBOL))
                    .append(self.result.into_syntax(interner))
                    .build(),
            )
            .build()
    }
}

#[derive(Clone, Debug)]
pub enum ReferenceType {
    FuncRef,
    ExternRef,
}

// pub struct FunctionTypes {
//     types: Vec<PrimitiveFunctionType>,
//     index: HashMap<PrimitiveFunctionType, u32>,
// }
