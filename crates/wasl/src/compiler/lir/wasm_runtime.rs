use std::sync::Mutex;

use lazy_static::lazy_static;
use wasmer::{sys::EngineBuilder, Singlepass, Store};

lazy_static! {
    /// A Wasm store that can be reused
    pub static ref STORE: Mutex<Store> = Mutex::new(Store::new(EngineBuilder::new(Singlepass::new()).engine()));
}
