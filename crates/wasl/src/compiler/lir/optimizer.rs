use super::Expression;
use crate::compiler::interner::Interner;
use crate::sexpr::Syntax;
use egg::*;
use lasso::Spur;
use noisy_float::types::{n32, n64, N32, N64};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
enum LIRExprLang {
    I32Const(i32),
    I64Const(i64),
    F32Const(N32),
    F64Const(N64),
    V128Const(i128),
    RefFunc(Symbol),
    RefNull,
    LocalGet(Symbol),
    LocalSet(Vec<Symbol>, Id),
    GlobalGet(Symbol),
    GlobalSet(Vec<Symbol>, Id),
    DirectFunctionCall { func: Symbol, args: Id },
    IndirectFunctionCall { func: Id, args: Id },
    Sequence(Vec<Id>),
    // InlineAsm(Vec<Syntax>, Id),
}

impl LIRExprLang {
    pub fn from_lir(lir: &Expression, int: &Interner) -> (Id, RecExpr<Self>) {
        let mut re = RecExpr::default();
        let id = LIRExprLang::id_from_lir(lir, &mut re, int);
        (id, re)
    }

    fn sequence_from_lir(lir: &[Expression], re: &mut RecExpr<Self>, int: &Interner) -> Id {
        let seq = LIRExprLang::Sequence(
            lir.iter()
                .map(|e| LIRExprLang::id_from_lir(e, re, int))
                .collect(),
        );
        re.add(seq)
    }

    fn id_from_lir(lir: &Expression, re: &mut RecExpr<Self>, int: &Interner) -> Id {
        let spur_to_symbol = |id: Spur| Symbol::new(int.borrow().resolve(&id));

        let lir_expr = match lir {
            &Expression::I32Const { value } => LIRExprLang::I32Const(value),
            &Expression::I64Const { value } => LIRExprLang::I64Const(value),
            &Expression::F32Const { value } => LIRExprLang::F32Const(n32(value)),
            &Expression::F64Const { value } => LIRExprLang::F64Const(n64(value)),
            &Expression::V128Const { value } => LIRExprLang::V128Const(value),
            &Expression::RefFunc { id } => LIRExprLang::RefFunc(spur_to_symbol(id)),
            &Expression::RefNull => LIRExprLang::RefNull,
            &Expression::LocalGet { id, .. } => LIRExprLang::LocalGet(spur_to_symbol(id)),
            Expression::LocalSet { ids, ref value, .. } => LIRExprLang::LocalSet(
                ids.iter().copied().map(spur_to_symbol).collect(),
                LIRExprLang::sequence_from_lir(value, re, int),
            ),
            &Expression::GlobalGet { id, .. } => LIRExprLang::GlobalGet(spur_to_symbol(id)),
            Expression::GlobalSet { ids, value, .. } => LIRExprLang::LocalSet(
                ids.iter().copied().map(spur_to_symbol).collect(),
                LIRExprLang::sequence_from_lir(value, re, int),
            ),
            Expression::IndirectFunctionCall {
                func, arguments, ..
            } => {
                let func = func
                    .iter()
                    .map(|e| LIRExprLang::id_from_lir(e, re, int))
                    .collect::<Vec<Id>>();
                let func = re.add(LIRExprLang::Sequence(func));

                LIRExprLang::IndirectFunctionCall {
                    func,
                    args: LIRExprLang::sequence_from_lir(arguments, re, int),
                }
            }
            Expression::DirectFunctionCall {
                func, arguments, ..
            } => LIRExprLang::DirectFunctionCall {
                func: spur_to_symbol(*func),
                args: LIRExprLang::sequence_from_lir(arguments, re, int),
            },
            Expression::Sequence { expressions } => LIRExprLang::Sequence(
                expressions
                    .iter()
                    .map(|e| LIRExprLang::id_from_lir(e, re, int))
                    .collect(),
            ),
            Expression::InlineAsm { asm, arguments, .. } => {
                unimplemented!();
                // LIRExprLang::InlineAsm(
                //     asm.clone(),
                //     LIRExprLang::sequence_from_lir(arguments, re, int),
                // )
            }
        };

        re.add(lir_expr)
    }
}

impl Language for LIRExprLang {
    fn matches(&self, other: &Self) -> bool {
        todo!()
    }

    fn children(&self) -> &[Id] {
        todo!()
    }

    fn children_mut(&mut self) -> &mut [Id] {
        todo!()
    }
}

// IndirectFunctionCall { func: RefFunc(sym), args } => DirectFuntionCall { func: sym, args }

// need a rewrite rule to turn a sequence of length one into a direct reference
// need a rewrite for joining two sequences

pub fn testing() {
    println!("{}", 5);
}
