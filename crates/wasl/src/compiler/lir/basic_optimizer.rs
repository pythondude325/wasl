use super::{Expression, FunctionDefinition, GlobalDefinition, Module};
use lasso::Spur;
use std::collections::HashMap;

// The basic optimizer has only a few possible rewrites:
// - Rewrite constant expressions with their evaluations
// - rewrite indirect function calls to direct function calls
// - flatten sequences

pub struct OptimizerContext<'m> {
    module: &'m Module,
    var_is_const: HashMap<Spur, bool>,
}

impl OptimizerContext<'_> {
    pub fn new<'m>(
        module: &'m Module,
        function: &FunctionDefinition,
        arg_const: Vec<bool>,
    ) -> OptimizerContext<'m> {
        let mut ctx = OptimizerContext {
            module,
            var_is_const: HashMap::new(),
        };

        // Could replace with this, but it's less understandable
        // let var_is_const = module.globals.iter()
        //     .map(|GlobalDefinition { id, mutable, .. }| (*id, !mutable))
        //     .chain(function.locals.iter().map(|(id, _)| (*id, false)))
        //     .chain(function.arg_ids.iter().cloned().zip(arg_const))
        //     .collect::<HashMap<Spur, bool>>();

        for GlobalDefinition { id, mutable, .. } in &module.globals {
            ctx.var_is_const.insert(*id, !mutable);
        }

        for (id, _) in &function.locals {
            ctx.var_is_const.insert(*id, false);
        }

        for (id, constness) in function.arg_ids.iter().zip(arg_const) {
            ctx.var_is_const.insert(*id, constness);
        }

        ctx
    }

    fn is_const(&mut self, expr: &Expression) -> bool {
        match expr {
            Expression::I32Const { .. }
            | Expression::I64Const { .. }
            | Expression::F32Const { .. }
            | Expression::F64Const { .. }
            | Expression::V128Const { .. }
            | Expression::RefFunc { .. }
            | Expression::RefNull => true,
            Expression::GlobalSet { .. } => false,
            Expression::LocalSet { .. } => false,
            Expression::GlobalGet { id, .. } | Expression::LocalGet { id, .. } => {
                self.var_is_const[id]
            }
            Expression::Sequence { expressions } => expressions.iter().all(|e| self.is_const(e)),
            Expression::InlineAsm { .. } => {
                // TODO: need way better analysis for this one i think
                unimplemented!()
            }
            Expression::IndirectFunctionCall {
                func, arguments, ..
            } => {
                // TODO: This should check if the function mutates stuff first, idk if that's possible tho
                self.is_const(func) && self.is_const(arguments)
            }
            Expression::DirectFunctionCall { arguments, .. } => {
                // TODO: This should check if the function mutates stuff first
                self.is_const(arguments)
            }
            // I think this is okay, but it's not like the branch returns anything anyways
            Expression::Branch { arguments, .. } => self.is_const(arguments),
            Expression::Block { value, .. } => self.is_const(value),
            Expression::If {
                condition,
                consequent,
                alternate,
                ..
            } => self.is_const(condition) && self.is_const(consequent) && self.is_const(alternate),
        }
    }

    pub fn optimize(&self, expr: &mut Expression) -> bool {
        match expr {
            Expression::Sequence { expressions } if expressions.len() == 1 => {
                // Unnest sequences with length of one
                let e = expressions.remove(0);
                *expr = e;
                true
            }
            Expression::Sequence { expressions }
                if expressions
                    .iter()
                    .any(|e| matches!(e, Expression::Sequence { .. })) =>
            {
                // Splice nested sequences
                let (i, es) = expressions
                    .iter()
                    .enumerate()
                    .find_map(|(i, e)| match e {
                        Expression::Sequence { expressions: es } => Some((i, es.clone())),
                        _ => None,
                    })
                    .unwrap();

                expressions.splice(i..i + 1, es);

                true
            }
            // Turn indirect function calls into direct function calls if they
            // are just a refence to a function
            Expression::IndirectFunctionCall {
                func,
                arguments,
                func_type,
            } if matches!(func.as_ref(), Expression::RefFunc { .. }) => match func.as_ref() {
                &Expression::RefFunc { id } => {
                    *expr = Expression::DirectFunctionCall {
                        func: id,
                        func_type: func_type.clone(),
                        arguments: arguments.clone(),
                    };
                    true
                }
                _ => unreachable!(),
            },
            expr => {
                // Optimize nested expressions
                match expr {
                    Expression::I32Const { .. }
                    | Expression::I64Const { .. }
                    | Expression::F32Const { .. }
                    | Expression::F64Const { .. }
                    | Expression::V128Const { .. }
                    | Expression::RefFunc { .. }
                    | Expression::RefNull
                    | Expression::LocalGet { .. }
                    | Expression::GlobalGet { .. }
                    | Expression::LocalSet { .. }
                    | Expression::GlobalSet { .. } => false,
                    Expression::Sequence { expressions } => {
                        expressions.iter_mut().any(|e| self.optimize(e))
                    }
                    Expression::IndirectFunctionCall {
                        func, arguments, ..
                    } => self.optimize(func) || self.optimize(arguments),
                    Expression::DirectFunctionCall { arguments, .. } => self.optimize(arguments),
                    Expression::InlineAsm { arguments, .. } => self.optimize(arguments),
                    Expression::Branch { arguments, .. } => self.optimize(arguments),
                    Expression::Block { value, .. } => self.optimize(value),
                    Expression::If {
                        condition,
                        consequent,
                        alternate,
                        ..
                    } => {
                        self.optimize(condition)
                            || self.optimize(consequent)
                            || self.optimize(alternate)
                    }
                }
            }
        }
    }

    // fn(&mut T, T -> T)
}
