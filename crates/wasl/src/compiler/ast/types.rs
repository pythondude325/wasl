use super::{ParserContext, SyntaxError, SyntaxResult};
use crate::common::*;

use crate::sexpr::{SExpr, Syntax};
use lasso::Spur;

#[derive(Clone, Debug)]
pub enum TypeKind {
    Variable(Spur),
    Function { param: Vec<Type>, result: Vec<Type> },
    Forall { vars: Vec<Spur>, ty: Box<Type> },
}

pub const FUNC_TYPE_SYMBOL: &str = "func";
pub const FUNC_TYPE_ALT_SYMBOL: &str = "->";
pub const FORALL_TYPE_SYMBOL: &str = "for";
pub const FORALL_TYPE_ALT_SYMBOL: &str = "∀";

pub type Type = Locatable<TypeKind>;

impl Type {
    /// Parse a type from s-expression
    ///
    /// Syntax:
    /// - `<symbol>`: named type
    pub fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<Type> {
        match source.sexpr() {
            SExpr::Symbol(id) => Ok(TypeKind::Variable(*id).at(source)),
            // SExpr::List(_items) => {
            //     unimplemented!()
            // }
            SExpr::List(l) => {
                let func_type_symbol = ctx.interner.static_symbol(FUNC_TYPE_SYMBOL);
                let func_type_alt_symbol = ctx.interner.static_symbol(FUNC_TYPE_ALT_SYMBOL);
                let forall_type_symbol = ctx.interner.static_symbol(FORALL_TYPE_SYMBOL);
                let forall_type_alt_symbol = ctx.interner.static_symbol(FORALL_TYPE_ALT_SYMBOL);

                match l.as_slice() {
                    &[syntax_pat!(SExpr::Symbol(func_kw)), syntax_pat!(SExpr::List(ref param)), syntax_pat!(SExpr::List(ref result))]
                        if func_kw == func_type_symbol || func_kw == func_type_alt_symbol =>
                    {
                        Ok(TypeKind::Function {
                            param: param
                                .iter()
                                .map(|s| Type::parse(s, ctx))
                                .collect::<SyntaxResult<Vec<Type>>>()?,
                            result: result
                                .iter()
                                .map(|s| Type::parse(s, ctx))
                                .collect::<SyntaxResult<Vec<Type>>>()?,
                        }
                        .at(source))
                    }
                    &[syntax_pat!(SExpr::Symbol(forall_kw)), syntax_pat!(SExpr::List(ref vars)), ref type_expr]
                        if forall_kw == forall_type_symbol
                            || forall_kw == forall_type_alt_symbol =>
                    {
                        let vars = vars
                            .iter()
                            .map(|v| match v.sexpr() {
                                &SExpr::Symbol(id) => Ok(id),
                                _ => Err(SyntaxError::InvalidForall.at(v)),
                            })
                            .collect::<SyntaxResult<Vec<Spur>>>()?;

                        Ok(TypeKind::Forall {
                            vars,
                            ty: Box::new(Type::parse(type_expr, ctx)?),
                        }
                        .at(source))
                    }
                    _ => Err(SyntaxError::InvalidType.at(source)),
                }
            }
            _ => Err(SyntaxError::InvalidType.at(source)),
        }
    }
}
