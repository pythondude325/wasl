use super::interner::Interner;
use super::mir::{self};
use crate::common::*;
use crate::sexpr::Syntax;
use crate::types::{self, Type};
use lasso::Spur;
use num::ToPrimitive;
use petgraph::graphmap::DiGraphMap;
use std::collections::HashMap;
use std::io;
use types::primitive::{PrimitiveFunctionType, ResultType, ValueType};

pub mod wasm_runtime;
// pub mod optimizer;
pub mod basic_optimizer;

pub const PREINTERN_SYMBOLS: &[&str] = &[
    MODULE_SYMBOL,
    EXPORT_SYMBOL,
    FUNC_SYMBOL,
    PARAM_SYMBOL,
    RESULT_SYMBOL,
    LOCAL_SYMBOL,
    GLOBAL_SYMBOL,
    MUT_SYMBOL,
    TYPE_SYMBOL,
    TABLE_SYMBOL,
    INSTR_I32_CONST_SYMBOL,
    INSTR_I64_CONST_SYMBOL,
    INSTR_F32_CONST_SYMBOL,
    INSTR_F64_CONST_SYMBOL,
    INSTR_V128_CONST_SYMBOL,
    INSTR_LOCAL_GET_SYMBOL,
    INSTR_LOCAL_SET_SYMBOL,
    INSTR_CALL_SYMBOL,
    INSTR_GLOBAL_GET_SYMBOL,
    INSTR_GLOBAL_SET_SYMBOL,
    INSTR_REF_FUNC_SYMBOL,
    INSTR_REF_NULL_SYMBOL,
    INSTR_TABLE_GET_SYMBOL,
    INSTR_TABLE_SET_SYMBOL,
    INSTR_CALL_INDIRECT_SYMBOL,
    INSTR_BR_SYMBOL,
    INSTR_BLOCK_SYMBOL,
    INSTR_LOOP_SYMBOL,
    INSTR_IF_SYMBOL,
    INSTR_ELSE_SYMBOL,
    INSTR_END_SYMBOL,
];

pub const MODULE_SYMBOL: &str = "module";
pub const EXPORT_SYMBOL: &str = "export";
pub const FUNC_SYMBOL: &str = "func";
pub const PARAM_SYMBOL: &str = "param";
pub const RESULT_SYMBOL: &str = "result";
pub const LOCAL_SYMBOL: &str = "local";
pub const GLOBAL_SYMBOL: &str = "global";
pub const MUT_SYMBOL: &str = "mut";
pub const TYPE_SYMBOL: &str = "type";
pub const TABLE_SYMBOL: &str = "table";
pub const INSTR_I32_CONST_SYMBOL: &str = "i32.const";
pub const INSTR_I64_CONST_SYMBOL: &str = "i64.const";
pub const INSTR_F32_CONST_SYMBOL: &str = "f32.const";
pub const INSTR_F64_CONST_SYMBOL: &str = "f64.const";
pub const INSTR_V128_CONST_SYMBOL: &str = "v128.const";
pub const INSTR_LOCAL_GET_SYMBOL: &str = "local.get";
pub const INSTR_LOCAL_SET_SYMBOL: &str = "local.set";
pub const INSTR_CALL_SYMBOL: &str = "call";
pub const INSTR_GLOBAL_GET_SYMBOL: &str = "global.get";
pub const INSTR_GLOBAL_SET_SYMBOL: &str = "global.set";
pub const INSTR_REF_FUNC_SYMBOL: &str = "ref.func";
pub const INSTR_REF_NULL_SYMBOL: &str = "ref.null";
pub const INSTR_TABLE_GET_SYMBOL: &str = "table.get";
pub const INSTR_TABLE_SET_SYMBOL: &str = "table.set";
pub const INSTR_CALL_INDIRECT_SYMBOL: &str = "call_indirect";
pub const INSTR_BR_SYMBOL: &str = "br";
pub const INSTR_BLOCK_SYMBOL: &str = "block";
pub const INSTR_LOOP_SYMBOL: &str = "loop";
pub const INSTR_IF_SYMBOL: &str = "if";
pub const INSTR_ELSE_SYMBOL: &str = "else";
pub const INSTR_END_SYMBOL: &str = "end";

#[derive(Clone, Debug)]
struct VariableAllocator {
    interner: Interner,
    pub slots: Vec<(Spur, ValueType)>,
    variables: Vec<(Spur, Vec<Spur>)>,
}

impl VariableAllocator {
    fn new(interner: &Interner) -> VariableAllocator {
        VariableAllocator {
            interner: interner.clone(),
            slots: Vec::new(),
            variables: Vec::new(),
        }
    }

    fn create_variable(&mut self, id: Spur, ty: &types::LocalType) {
        let primitive_type = ty.as_primitive();

        let mut var_ids = Vec::new();

        for valtype in primitive_type {
            let id = self.interner.generate_symbol("$var-");

            var_ids.push(id);
            self.slots.push((id, valtype));
        }

        self.variables.push((id, var_ids));
    }

    fn lookup_var(&self, id: Spur) -> Option<&[Spur]> {
        self.variables
            .iter()
            .rev()
            .find(|(var_id, _)| id == *var_id)
            .map(|(_, var_ids)| var_ids.as_ref())
    }

    fn unscope_var(&mut self, id: Spur) {
        if let Some((index, _)) = self
            .variables
            .iter()
            .enumerate()
            .rev()
            .find(|(_, (var_id, _))| id == *var_id)
        {
            self.variables.remove(index);
        }
    }
}

#[derive(Clone, Debug)]
pub enum ConstValue {
    I32(i32),
    I64(i64),
    F32(f32),
    F64(f64),
    V128(i128),
    FuncRef(Spur),
    Null,
}

impl ConstValue {
    pub fn into_syntax(&self, interner: &Interner) -> Syntax {
        match self {
            &ConstValue::I32(value) => Expression::I32Const { value }.into_syntax(interner),
            &ConstValue::I64(value) => Expression::I64Const { value }.into_syntax(interner),
            &ConstValue::F32(value) => Expression::F32Const { value }.into_syntax(interner),
            &ConstValue::F64(value) => Expression::F64Const { value }.into_syntax(interner),
            &ConstValue::V128(value) => Expression::V128Const { value }.into_syntax(interner),
            &ConstValue::FuncRef(id) => Expression::RefFunc { id }.into_syntax(interner),
            ConstValue::Null => Expression::RefNull.into_syntax(interner),
        }
    }

    pub fn valuetype(&self) -> ValueType {
        match self {
            ConstValue::I32(_) => ValueType::I32,
            ConstValue::I64(_) => ValueType::I64,
            ConstValue::F32(_) => ValueType::F32,
            ConstValue::F64(_) => ValueType::F64,
            ConstValue::V128(_) => ValueType::V128,
            ConstValue::Null | ConstValue::FuncRef(_) => ValueType::FuncRef,
        }
    }
}

struct ExprContext<'m> {
    module: &'m Module,
    pub locals: VariableAllocator,
}

impl ExprContext<'_> {
    fn new(module: &Module) -> ExprContext {
        ExprContext {
            module,
            locals: VariableAllocator::new(&module.interner),
        }
    }

    fn generate_expr(&mut self, mir: &mir::TypedExpr) -> Expression {
        match &mir.data {
            mir::TypedExprKind::IntLiteral { int, int_type } => match int_type {
                &types::LocalType::Memory(types::MemoryType::Integer(types::IntegerType::I8)) => {
                    Expression::I32Const {
                        value: int.to_i8().unwrap() as i32,
                    }
                }
                &types::LocalType::Memory(types::MemoryType::Integer(types::IntegerType::U8)) => {
                    Expression::I32Const {
                        value: int.to_u8().unwrap() as i32,
                    }
                }
                &types::LocalType::Memory(types::MemoryType::Integer(types::IntegerType::I16)) => {
                    Expression::I32Const {
                        value: int.to_i16().unwrap() as i32,
                    }
                }
                &types::LocalType::Memory(types::MemoryType::Integer(types::IntegerType::U16)) => {
                    Expression::I32Const {
                        value: int.to_u16().unwrap() as i32,
                    }
                }
                &types::LocalType::Memory(types::MemoryType::Integer(types::IntegerType::I32)) => {
                    Expression::I32Const {
                        value: int.to_i32().unwrap(),
                    }
                }
                &types::LocalType::Memory(types::MemoryType::Integer(types::IntegerType::U32)) => {
                    Expression::I32Const {
                        value: int.to_u32().unwrap() as i32,
                    }
                }
                &types::LocalType::Memory(types::MemoryType::Integer(types::IntegerType::I64)) => {
                    Expression::I64Const {
                        value: int.to_i64().unwrap(),
                    }
                }
                &types::LocalType::Memory(types::MemoryType::Integer(types::IntegerType::U64)) => {
                    Expression::I64Const {
                        value: int.to_u64().unwrap() as i64,
                    }
                }
                _ => unimplemented!(),
            },
            mir::TypedExprKind::FloatLiteral { float, float_type } => match float_type {
                &types::LocalType::Memory(types::MemoryType::Float(types::FloatType::F32)) => {
                    Expression::F32Const {
                        value: *float as f32,
                    }
                }
                &types::LocalType::Memory(types::MemoryType::Float(types::FloatType::F64)) => {
                    Expression::F64Const { value: *float }
                }
                _ => unimplemented!(),
            },
            mir::TypedExprKind::InlineAsmCall(inline_asm_call) => {
                // TODO: we need to allocate locals to the inline asm here
                Expression::InlineAsm {
                    asm: inline_asm_call.asm.clone(),
                    ty: inline_asm_call.result.as_primitive(),
                    arguments: Box::new(
                        inline_asm_call
                            .params
                            .iter()
                            .map(|te| self.generate_expr(te))
                            .collect(),
                    ),
                }
            }
            mir::TypedExprKind::VariableLoad(mir::VariableLoad::Function { id, .. }) => {
                Expression::RefFunc {
                    id: self.module.functions[id].id,
                }
            }
            mir::TypedExprKind::VariableLoad(mir::VariableLoad::Global { id, .. }) => {
                let ids = self.module.global_varallocator.lookup_var(*id).unwrap(); // TODO: impl error

                ids.iter()
                    .map(|id| Expression::GlobalGet {
                        ty: self
                            .module
                            .global_varallocator
                            .slots
                            .iter()
                            .find_map(|(var_id, ty)| if *id == *var_id { Some(*ty) } else { None })
                            .unwrap(),
                        id: *id,
                    })
                    .collect()
            }
            mir::TypedExprKind::VariableLoad(mir::VariableLoad::Local { id, var_type }) => {
                if let Some(var_ids) = self.locals.lookup_var(*id) {
                    var_ids
                        .iter()
                        .cloned()
                        .zip(var_type.as_primitive())
                        .map(|(id, ty)| Expression::LocalGet { ty, id })
                        .collect()
                } else {
                    panic!() // TODO: make this an error
                }
            }
            mir::TypedExprKind::VariableStore(mir::VariableStore::Global { id, value }) => {
                let var_ids = self.module.global_varallocator.lookup_var(*id).unwrap(); // TODO: error
                let var_type = value.as_single_type().unwrap();

                std::iter::once(self.generate_expr(value.as_ref()))
                    .chain(
                        var_ids
                            .iter()
                            .cloned()
                            .zip(var_type.as_primitive())
                            .map(|(id, ty)| Expression::GlobalSet { ty, id }),
                    )
                    .collect()
            }
            mir::TypedExprKind::VariableStore(mir::VariableStore::Local { id, value }) => {
                let var_ids = self.locals.lookup_var(*id).unwrap().to_vec(); // TODO: error
                let var_type = value.as_single_type().unwrap();
                std::iter::once(self.generate_expr(value.as_ref()))
                    .chain(
                        var_ids
                            .iter()
                            .cloned()
                            .zip(var_type.as_primitive())
                            .map(|(id, ty)| Expression::LocalSet { ty, id }),
                    )
                    .collect()
            }
            mir::TypedExprKind::FunctionCall { func, params, .. } => {
                let func_expr = self.generate_expr(func);

                let func_type =
                    if let Some(types::LocalType::Table(types::TableType::FuncRef(func_ref))) =
                        func.as_single_type()
                    {
                        PrimitiveFunctionType::from_funcref(&func_ref)
                    } else {
                        panic!() // TODO: should error
                    };

                Expression::IndirectFunctionCall {
                    func: Box::new(func_expr),
                    func_type,
                    arguments: Box::new(params.iter().map(|te| self.generate_expr(te)).collect()),
                }
            } // mir::TypedExprKind::DirectFunctionCall { .. } => {
              //     todo!() // TODO: will remove direct function call
              // }
        }
    }
}

#[derive(Clone, Debug)]
pub struct Module {
    interner: Interner,
    pub functions: HashMap<Spur, FunctionDefinition>,
    // function_imports: Vec<()>,
    global_varallocator: VariableAllocator,
    globals: Vec<GlobalDefinition>,
    // global_imports: Vec<()>,
    exports: Vec<ExportDefinition>,
}

impl Module {
    pub fn new(interner: &Interner) -> Module {
        Module {
            interner: interner.clone(),
            functions: HashMap::new(),
            // function_imports: Vec::new(),
            global_varallocator: VariableAllocator::new(interner),
            globals: Vec::new(),
            // global_imports: Vec::new(),
            exports: Vec::new(),
        }
    }

    pub fn output_module(&self) -> Syntax {
        let table_ind_sym = self
            .interner
            .borrow_mut()
            .get_or_intern_static("$table-ind");

        Syntax::build_list()
            .sym(self.interner.static_symbol(MODULE_SYMBOL))
            .syntax(
                Syntax::build_list()
                    .sym(self.interner.static_symbol(TABLE_SYMBOL))
                    .sym(table_ind_sym)
                    .int(1_i32)
                    .int(1_i32)
                    .sym(
                        self.interner
                            .static_symbol(types::primitive::PRIMITIVE_FUNCREF_SYMBOL),
                    )
                    .build(),
            )
            .append(self.globals.iter().map(|g| g.into_syntax(&self.interner)))
            .append(
                self.functions
                    .values()
                    .map(|func| func.into_syntax(&self.interner)),
            )
            .append(
                self.exports
                    .iter()
                    .map(|export| export.into_syntax(&self.interner)),
            )
            .build()
    }

    fn eval_const(&self, expr: &mir::TypedExpr) -> Vec<ConstValue> {
        let mut ctx = ExprContext::new(self);

        let expr_func_body = ctx.generate_expr(expr);

        let expr_func_type = PrimitiveFunctionType {
            param: ResultType::UNIT,
            result: expr.as_multi_type().as_primitive(),
        };

        let func_id = ctx.module.interner.generate_symbol("$const-eval-func-");

        let expr_func = FunctionDefinition {
            id: func_id,
            func_type: expr_func_type,
            arg_ids: Vec::new(),
            locals: ctx.locals.slots.clone(),
            value: expr_func_body,
        };

        let mut eval_module = self.clone();
        eval_module.functions.insert(func_id, expr_func);
        eval_module.exports.push(ExportDefinition::FunctionExport {
            function: func_id,
            export_name: String::from("const-eval"),
        });

        let mut possible_funcs = Vec::new();
        for (_, func_def) in self.functions.iter() {
            possible_funcs.push(func_def.id);
            eval_module.exports.push(ExportDefinition::FunctionExport {
                function: func_def.id,
                export_name: String::from(self.interner.borrow().resolve(&func_def.id)),
            });
        }

        let mut wat_buffer = io::Cursor::new(Vec::new());
        eval_module
            .output_module()
            .write(&mut wat_buffer, &ctx.module.interner)
            .unwrap();

        // println!("{}", std::str::from_utf8(wat_buffer.get_ref()).unwrap());

        let mut store = wasm_runtime::STORE
            .lock()
            .expect("Failed to lock store mutex");

        let wasm_module_result = wasmer::Module::new(&mut store, wat_buffer.get_ref());

        let module = match wasm_module_result {
            Err(e) => {
                println!("{}", e);
                panic!();
            }
            Ok(m) => m,
        };

        let import_object = wasmer::imports! {};

        let wasm_instance = wasmer::Instance::new(&mut store, &module, &import_object).unwrap();

        let wasm_results = wasm_instance
            .exports
            .get_function("const-eval")
            .unwrap()
            .call(&mut store, &[])
            .unwrap();

        wasm_results
            .as_ref()
            .iter()
            .map(|result| match result {
                &wasmer::Value::I32(v) => ConstValue::I32(v),
                &wasmer::Value::I64(v) => ConstValue::I64(v),
                &wasmer::Value::F32(v) => ConstValue::F32(v),
                &wasmer::Value::F64(v) => ConstValue::F64(v),
                &wasmer::Value::V128(v) => ConstValue::V128(v as i128),
                wasmer::Value::FuncRef(f) => {
                    if let Some(f) = f {
                        ConstValue::FuncRef(
                            possible_funcs
                                .iter()
                                .copied()
                                .find(|id| {
                                    let func = wasm_instance
                                        .exports
                                        .get_function(self.interner.borrow().resolve(id))
                                        .unwrap();

                                    f == func
                                })
                                .unwrap(),
                        )
                    } else {
                        ConstValue::Null
                    }
                }
                &wasmer::Value::ExternRef(_) => {
                    panic!() // TODO: write a proper error
                }
            })
            .collect()

        // dbg!(&wasm_results);
    }

    fn add_global(&mut self, mir_global: &mir::GlobalDefinition) {
        self.global_varallocator
            .create_variable(mir_global.name, &mir_global.global_type);

        let var_ids = self
            .global_varallocator
            .lookup_var(mir_global.name)
            .unwrap();

        let values = self.eval_const(&mir_global.body);

        for (id, val) in var_ids.iter().copied().zip(values) {
            self.globals.push(GlobalDefinition {
                id,
                mutable: mir_global.mutable,
                global_type: val.valuetype(),
                value: val,
            })
        }
    }

    pub fn add_function(&mut self, mir_func: &mir::FunctionDefinition) {
        let mut expr_ctx = ExprContext::new(self);

        for (arg_id, arg_type) in mir_func
            .argument_ids
            .iter()
            .zip(mir_func.type_signature.param.as_ref())
        {
            expr_ctx.locals.create_variable(*arg_id, arg_type);
        }

        let local_split = expr_ctx.locals.slots.len();

        let body = expr_ctx.generate_expr(&mir_func.body);

        let (args, locals) = expr_ctx.locals.slots.split_at(local_split);

        let func_type = PrimitiveFunctionType {
            param: args.iter().map(|(_, ty)| *ty).collect(),
            result: mir_func.type_signature.result.as_primitive(),
        };

        let func_id = self.interner.generate_symbol("$func-");

        let func = FunctionDefinition {
            id: func_id,
            arg_ids: args.iter().map(|(id, _)| *id).collect(),
            func_type,
            locals: locals.to_owned(),
            value: body,
        };

        self.functions.insert(mir_func.name, func);
    }

    pub fn from_mir(interner: &Interner, mir_module: &mir::Module) -> Module {
        let mut module = Module::new(interner);

        let mut dependency_graph = DiGraphMap::<Spur, Location>::new();

        for (item_id, item) in &mir_module.items {
            match &item.data {
                mir::ModuleItemKind::Global(const_def) => {
                    dependency_graph.add_node(*item_id);
                    analyze_expr_dependencies(&const_def.body, *item_id, &mut dependency_graph);
                }
                mir::ModuleItemKind::Function(func_def) => {
                    dependency_graph.add_node(*item_id);
                    analyze_expr_dependencies(&func_def.body, *item_id, &mut dependency_graph);
                }
            }
        }

        // TODO: i need to redo this section and base it on a SCC algorithm instead
        // I should process the module graph one SCC at a time. Functoins are
        // allowed to have cycles, but globals may not.

        let item_order = match petgraph::algo::toposort(&dependency_graph, None) {
            Ok(order) => order,
            Err(_cycle) => {
                unimplemented!() // TODO: give this the right error
                                 // return Err(CodegenError::CircularDependency.at(ctx.items.lookup(cycle.node_id())));
            }
        };

        for item_id in item_order {
            let item = &mir_module.items[&item_id];

            match &item.data {
                mir::ModuleItemKind::Function(f) => {
                    module.add_function(f);
                }
                mir::ModuleItemKind::Global(g) => {
                    module.add_global(g);
                }
            }
        }

        for (export_item, export_name) in &mir_module.exports {
            let item = &mir_module.items[export_item];

            match &item.data {
                mir::ModuleItemKind::Function(_) => {
                    module.exports.push(ExportDefinition::FunctionExport {
                        function: module.functions[export_item].id,
                        export_name: interner.borrow().resolve(export_name).to_owned(),
                    })
                }
                mir::ModuleItemKind::Global(_) => {
                    let global_ids = module.global_varallocator.lookup_var(*export_item).unwrap();

                    if global_ids.len() != 1 {
                        panic!("may not export a non-primitive global"); // TODO: write a proper error
                    }

                    module.exports.push(ExportDefinition::GlobalExport {
                        global: global_ids[0],
                        export_name: interner.borrow().resolve(export_name).to_owned(),
                    })
                }
            }
        }

        module
    }

    pub fn optimize(&mut self) {
        let functions = self.functions.keys().cloned().collect::<Vec<Spur>>();

        for func_id in &functions {
            let mut new_func = self.functions[func_id].clone();
            new_func.optimize(self);
            self.functions.insert(*func_id, new_func);
        }
    }
}

fn analyze_expr_dependencies(
    expr: &mir::TypedExpr,
    parent: Spur,
    depgraph: &mut DiGraphMap<Spur, Location>,
) {
    match &expr.data {
        mir::TypedExprKind::IntLiteral { .. } | mir::TypedExprKind::FloatLiteral { .. } => {
            // No dependencies in these
        }
        mir::TypedExprKind::InlineAsmCall(mir::InlineAsmCall { params, .. }) => {
            for param in params {
                analyze_expr_dependencies(param, parent, depgraph);
            }
        }
        mir::TypedExprKind::FunctionCall { func, params, .. } => {
            analyze_expr_dependencies(func.as_ref(), parent, depgraph);
            for param in params {
                analyze_expr_dependencies(param, parent, depgraph);
            }
        }
        mir::TypedExprKind::VariableLoad(vl) => match vl {
            &mir::VariableLoad::Global { id, .. } => {
                depgraph.add_edge(id, parent, expr.as_location());
            }
            &mir::VariableLoad::Function { id, .. } => {
                depgraph.add_edge(id, parent, expr.as_location());
            }
            mir::VariableLoad::Local { .. } => {}
        },
        mir::TypedExprKind::VariableStore(vs) => match vs {
            &mir::VariableStore::Global { id, ref value } => {
                analyze_expr_dependencies(value, parent, depgraph);
                depgraph.add_edge(id, parent, expr.as_location());
            }
            mir::VariableStore::Local { value, .. } => {
                analyze_expr_dependencies(value, parent, depgraph);
            }
        },
    }
}

#[derive(Clone, Debug)]
pub enum ExportDefinition {
    GlobalExport { global: Spur, export_name: String },
    FunctionExport { function: Spur, export_name: String },
}

impl ExportDefinition {
    fn into_syntax(&self, interner: &Interner) -> Syntax {
        match self {
            ExportDefinition::GlobalExport {
                global,
                export_name,
            } => Syntax::build_list()
                .sym(interner.static_symbol(EXPORT_SYMBOL))
                .string(export_name)
                .syntax(
                    Syntax::build_list()
                        .sym(interner.static_symbol(GLOBAL_SYMBOL))
                        .sym(*global)
                        .build(),
                )
                .build(),
            ExportDefinition::FunctionExport {
                function,
                export_name,
            } => Syntax::build_list()
                .sym(interner.static_symbol(EXPORT_SYMBOL))
                .string(export_name)
                .syntax(
                    Syntax::build_list()
                        .sym(interner.static_symbol(FUNC_SYMBOL))
                        .sym(*function)
                        .build(),
                )
                .build(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct GlobalDefinition {
    id: Spur,
    mutable: bool,
    global_type: ValueType,
    value: ConstValue,
}

impl GlobalDefinition {
    fn into_syntax(&self, interner: &Interner) -> Syntax {
        Syntax::build_list()
            .sym(interner.static_symbol(GLOBAL_SYMBOL))
            .sym(self.id)
            .syntax(if self.mutable {
                Syntax::build_list()
                    .sym(interner.static_symbol(MUT_SYMBOL))
                    .syntax(self.global_type.into_syntax(interner))
                    .build()
            } else {
                self.global_type.into_syntax(interner)
            })
            .append(self.value.into_syntax(interner))
            .build()
    }
}

#[derive(Clone, Debug)]
pub struct FunctionDefinition {
    id: Spur,
    func_type: PrimitiveFunctionType,
    arg_ids: Vec<Spur>,
    locals: Vec<(Spur, ValueType)>,
    value: Expression,
}

impl FunctionDefinition {
    pub fn into_syntax(&self, interner: &Interner) -> Syntax {
        Syntax::build_list()
            .sym(interner.static_symbol(FUNC_SYMBOL))
            .sym(self.id)
            .append(
                self.arg_ids
                    .iter()
                    .zip(&self.func_type.param)
                    .map(|(id, ty)| {
                        Syntax::build_list()
                            .sym(interner.static_symbol(PARAM_SYMBOL))
                            .sym(*id)
                            .syntax(ty.into_syntax(interner))
                            .build()
                    }),
            )
            .syntax(
                Syntax::build_list()
                    .sym(interner.static_symbol(RESULT_SYMBOL))
                    .append(self.func_type.result.into_syntax(interner))
                    .build(),
            )
            .append(self.locals.iter().map(|&(id, ty)| {
                Syntax::build_list()
                    .sym(interner.static_symbol(LOCAL_SYMBOL))
                    .sym(id)
                    .syntax(ty.into_syntax(interner))
                    .build()
            }))
            .append(self.value.into_syntax(interner))
            .build()
    }

    pub fn optimize(&mut self, module: &Module) {
        let optimizer =
            basic_optimizer::OptimizerContext::new(module, self, vec![false; self.arg_ids.len()]);

        // println!("start: {:#?}", self);
        while optimizer.optimize(&mut self.value) {
            // println!("modification: {:#?}", self);
        }
        // println!("end: {:#?}", self);
    }
}

#[derive(Clone, Debug)]
pub struct TableDefinition {
    id: Spur,
    lower_limit: u32,
    upper_limit: u32,
    reftype: types::primitive::ReferenceType,
}

#[derive(Clone, Debug)]
pub enum Expression {
    LocalGet {
        ty: ValueType,
        id: Spur,
    },
    LocalSet {
        ty: ValueType,
        id: Spur,
    },
    GlobalGet {
        ty: ValueType,
        id: Spur,
    },
    GlobalSet {
        ty: ValueType,
        id: Spur,
    },
    I32Const {
        value: i32,
    },
    I64Const {
        value: i64,
    },
    F32Const {
        value: f32,
    },
    F64Const {
        value: f64,
    },
    V128Const {
        value: i128,
    },
    RefFunc {
        id: Spur,
    },
    RefNull,
    IndirectFunctionCall {
        func: Box<Expression>,
        func_type: PrimitiveFunctionType,
        arguments: Box<Expression>,
    },
    DirectFunctionCall {
        func: Spur,
        func_type: PrimitiveFunctionType,
        arguments: Box<Expression>,
    },
    InlineAsm {
        asm: Vec<Syntax>,
        ty: ResultType,
        arguments: Box<Expression>,
    },
    Sequence {
        expressions: Vec<Expression>,
    },
    Branch {
        id: Spur,
        arguments: Box<Expression>,
    },
    Block {
        id: Spur,
        block_type: ResultType,
        value: Box<Expression>,
    },
    // TODO: The big issue here:
    // Loop has a branch type which are passed as inputs to the loop, but the
    // LIR structure i use does not allow for me to take in values from an outer
    // structure, so i might need to immediately set those values to a local and
    // and then let instructions get those locals
    // Instruction provides a set of names for it to immediately `local.set` to
    // Loop {
    //     id: Spur,
    //     branch_type: ResultType,
    //     block_type: ResultType,
    //     value: Box<Expression>,
    // },
    If {
        id: Spur,
        block_type: ResultType,
        condition: Box<Expression>,
        consequent: Box<Expression>,
        alternate: Box<Expression>,
    },
}

impl Expression {
    // fn typecheck(&self) -> Option<(ResultType, ResultType)> {
    //     match self {
    //         Expression::I32Const { .. } => {
    //             Some((ResultType::UNIT, ResultType::from(ValueType::I32)))
    //         }
    //         Expression::I64Const { .. } => {
    //             Some((ResultType::UNIT, ResultType::from(ValueType::I64)))
    //         }
    //         Expression::F32Const { .. } => {
    //             Some((ResultType::UNIT, ResultType::from(ValueType::F32)))
    //         }
    //         Expression::F64Const { .. } => {
    //             Some((ResultType::UNIT, ResultType::from(ValueType::F64)))
    //         }
    //         Expression::V128Const { .. } => {
    //             Some((ResultType::UNIT, ResultType::from(ValueType::V128)))
    //         }
    //         Expression::LocalGet { ty, .. } => Some((ResultType::UNIT, ResultType::from(*ty))),
    //         Expression::LocalSet { ty, .. } => Some((ResultType::from(*ty), ResultType::UNIT)),
    //         Expression::GlobalGet { ty, .. } => Some((ResultType::from(*ty), ResultType::UNIT)),
    //         Expression::RefNull | Expression::RefFunc { .. } => {
    //             Some((ResultType::UNIT, ResultType::from(ValueType::FuncRef)))
    //         }
    //         Expression::IndirectFunctionCall { func_type, .. } => func_type.result.clone(),
    //         Expression::DirectFunctionCall { func_type, .. } => func_type.result.clone(),
    //         Expression::InlineAsm { ty, .. } => ty.clone(),
    //         Expression::Sequence { expressions } => {
    //             expressions.iter().flat_map(|ex| ex.result_type()).collect()
    //         }
    //         _ => unimplemented!(),
    //     }
    // }

    fn into_syntax(&self, interner: &Interner) -> Syntax {
        match self {
            &Expression::I32Const { value } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_I32_CONST_SYMBOL))
                .int(value)
                .build(),
            &Expression::I64Const { value } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_I64_CONST_SYMBOL))
                .int(value)
                .build(),
            &Expression::F32Const { value } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_F32_CONST_SYMBOL))
                .float(value)
                .build(),
            &Expression::F64Const { value } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_F64_CONST_SYMBOL))
                .float(value)
                .build(),
            &Expression::V128Const { .. } => unimplemented!(), // TODO: implement v128.const expression
            &Expression::LocalGet { id, .. } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_LOCAL_GET_SYMBOL))
                .sym(id)
                .build(),
            Expression::LocalSet { id, .. } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_LOCAL_SET_SYMBOL))
                .sym(*id)
                .build(),
            &Expression::GlobalGet { id: global, .. } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_GLOBAL_GET_SYMBOL))
                .sym(global)
                .build(),
            Expression::GlobalSet { id, .. } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_GLOBAL_SET_SYMBOL))
                .sym(*id)
                .build(),
            &Expression::RefFunc { id } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_REF_FUNC_SYMBOL))
                .sym(id)
                .build(),
            Expression::RefNull => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_REF_NULL_SYMBOL))
                .build(),
            Expression::IndirectFunctionCall {
                func,
                func_type,
                arguments,
                ..
            } => {
                let table_ind_sym = interner.borrow_mut().get_or_intern_static("$table-ind");

                Syntax::build_list()
                    .append(arguments.into_syntax(interner))
                    .append((Expression::I32Const { value: 0 }).into_syntax(interner))
                    .append(func.into_syntax(interner))
                    .sym(interner.static_symbol(INSTR_TABLE_SET_SYMBOL))
                    .sym(table_ind_sym) // TODO: this should be passed in somehow
                    .syntax((Expression::I32Const { value: 0 }).into_syntax(interner))
                    .sym(interner.static_symbol(INSTR_CALL_INDIRECT_SYMBOL))
                    .sym(table_ind_sym)
                    .syntax(
                        Syntax::build_list()
                            .sym(interner.static_symbol(PARAM_SYMBOL))
                            .append(func_type.param.into_syntax(interner))
                            .build(),
                    )
                    .syntax(
                        Syntax::build_list()
                            .sym(interner.static_symbol(RESULT_SYMBOL))
                            .append(func_type.result.into_syntax(interner))
                            .build(),
                    )
                    .build()
            }
            Expression::DirectFunctionCall {
                func, arguments, ..
            } => Syntax::build_list()
                .append(arguments.into_syntax(interner))
                .sym(interner.static_symbol(INSTR_CALL_SYMBOL))
                .sym(*func)
                .build(),
            Expression::InlineAsm { asm, arguments, .. } => Syntax::build_list()
                .append(arguments.into_syntax(interner))
                .append(asm.iter().cloned())
                .build(),
            Expression::Sequence { expressions } => Syntax::build_list()
                .append(expressions.iter().flat_map(|e| e.into_syntax(interner)))
                .build(),
            Expression::Branch { id, arguments } => Syntax::build_list()
                .append(arguments.into_syntax(interner))
                .sym(interner.static_symbol(INSTR_BR_SYMBOL))
                .sym(*id)
                .build(),
            Expression::Block {
                id,
                block_type,
                value,
            } => Syntax::build_list()
                .sym(interner.static_symbol(INSTR_BLOCK_SYMBOL))
                .sym(*id)
                .syntax(
                    Syntax::build_list()
                        .sym(interner.static_symbol(RESULT_SYMBOL))
                        .append(block_type.into_syntax(interner))
                        .build(),
                )
                .append(value.into_syntax(interner))
                .sym(interner.static_symbol(INSTR_END_SYMBOL))
                .build(),
            // Expression::Loop {
            //     id,
            //     branch_type,
            //     block_type,
            //     value,
            // } => Syntax::build_list()
            //     .sym(interner.static_symbol(INSTR_LOOP_SYMBOL))
            //     .sym(*id)
            Expression::If {
                id,
                block_type,
                condition,
                consequent,
                alternate,
            } => Syntax::build_list()
                .append(condition.into_syntax(interner))
                .sym(interner.static_symbol(INSTR_IF_SYMBOL))
                .sym(*id)
                .syntax(
                    Syntax::build_list()
                        .sym(interner.static_symbol(RESULT_SYMBOL))
                        .append(block_type.into_syntax(interner))
                        .build(),
                )
                .append(consequent.into_syntax(interner))
                .sym(interner.static_symbol(INSTR_ELSE_SYMBOL))
                .append(alternate.into_syntax(interner))
                .sym(interner.static_symbol(INSTR_END_SYMBOL))
                .build(),
            // Expression::Loop { .. } => todo!(),
        }
    }

    // TODO: make LIR optimizer with egg
    // Main rewrite rules should be:
    // - IndirectFunctionCall -> DirectFunctionCall
    // - Inline Function call
    // - Constant expression evaluation
}

impl FromIterator<Expression> for Expression {
    fn from_iter<T: IntoIterator<Item = Expression>>(iter: T) -> Self {
        Expression::Sequence {
            expressions: iter.into_iter().collect(),
        }
    }
}
