use lasso::Spur;
use std::collections::HashMap;
use std::rc::Rc;

#[derive(Clone, Debug)]
pub struct Scope<V: Clone> {
    pub variables: HashMap<Spur, V>,
    pub parent: Option<Rc<Scope<V>>>,
}

impl<V: Clone> Default for Scope<V> {
    fn default() -> Self {
        Self::new()
    }
}

impl<V: Clone> Scope<V> {
    pub fn new() -> Scope<V> {
        Scope {
            variables: HashMap::new(),
            parent: None,
        }
    }

    pub fn new_from(parent: &Rc<Scope<V>>) -> Scope<V> {
        Scope {
            variables: HashMap::new(),
            parent: Some(parent.clone()),
        }
    }

    // pub fn is_superscope(a: &Rc<Scope<V>>, b: &Rc<Scope<V>>) -> bool {
    //     if Rc::ptr_eq(a, b) {
    //         true
    //     } else if let Some(parent) = &a.parent {
    //         Scope::is_superscope(parent, b)
    //     } else {
    //         false
    //     }
    // }

    // pub fn find_scope(&self, id: Spur) -> Option<&Scope<V>> {
    //     if let Some(_) = self.variables.get(&id) {
    //         Some(self)
    //     } else {
    //         self.parent?.find_scope(id)
    //     }
    // }

    pub fn lookup(&self, id: Spur) -> Option<&V> {
        if let Some(val) = self.variables.get(&id) {
            Some(val)
        } else if let Some(parent) = &self.parent {
            parent.lookup(id)
        } else {
            None
        }
    }
}
