use std::collections::HashMap;

use self::typecheck::Typechecker;

use super::ast;

use super::interner::Interner;
use super::scope::Scope;
use crate::common::*;
use crate::sexpr;
use crate::types;

use lasso::Spur;
use petgraph::graphmap::DiGraphMap;
use std::rc::Rc;
use thiserror::Error;

mod typecheck;

#[derive(Clone, Debug, Error)]
pub enum SemanticError {
    /// A cycle in type aliases was detected.
    #[error("A cycle was detected in type aliases")]
    CycleInTypeAlias,

    /// Unbound variable
    #[error("Unbound variable")]
    UnboundVariable,

    /// Variable was defined twice
    #[error("Variable was defined twice")]
    ReboundVariable,

    /// Function call callee must be a function
    #[error("Function callee must be a function")]
    CalleeWasNotFunction,

    /// Could not find suitable type for expression. Expected {expected:?} but found {found:?}
    #[error("Could not find a suitable type. Expected {expected:?} but found {found:?}")]
    UnexpectedType {
        expected: typecheck::Type,
        found: typecheck::Type,
    },

    /// Type used in expression context
    #[error("Type used in an expression context")]
    TypeUsedInExpressionContext,

    /// Cannot set constant variable
    #[error("cannot set the value of an immutable variable")]
    SetImmutable,

    #[error("Type variable was found without a qualifier")]
    UnqualifiedTypeVariable,

    #[error("Type was not a monotype")]
    NotMonotype,
}

type SemanticResult<T> = LResult<T, SemanticError>;

#[derive(Clone, Debug, PartialEq)]
pub enum VariableKind {
    Type(typecheck::Type),
    Local(typecheck::Type),
    Global { ty: types::LocalType, mutable: bool },
    Function(typecheck::Type),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Variable {
    Builtin(VariableKind),
    Source(Locatable<VariableKind>),
}

impl Variable {
    pub fn variable(&self) -> &VariableKind {
        match self {
            Variable::Builtin(kind) => kind,
            Variable::Source(locatable_pat!(kind)) => kind,
        }
    }

    pub fn location(&self) -> Option<&Location> {
        match self {
            Variable::Builtin(_) => None,
            Variable::Source(Locatable { location, .. }) => Some(location),
        }
    }
}

type VariableScope = Scope<Variable>;

impl VariableScope {
    fn lookup_type(&self, id: Spur) -> Option<&typecheck::Type> {
        if let Some(VariableKind::Type(t)) = self.lookup(id).map(Variable::variable) {
            Some(t)
        } else {
            None
        }
    }

    pub fn make_builtins_scope(interner: &Interner) -> Rc<Scope<Variable>> {
        let mut interner = interner.borrow_mut();
        let mut variables = HashMap::<Spur, Variable>::new();

        for (name, localtype) in types::BUILTIN_TYPES {
            let symbol = interner.get_or_intern_static(name);

            let overriden = variables
                .insert(
                    symbol,
                    Variable::Builtin(VariableKind::Type(typecheck::Type::Monotype(
                        localtype.clone(),
                    ))),
                )
                .is_some();

            assert!(
                !overriden,
                "type {name} got inserted into the builtins twice"
            );
        }

        Rc::new(Scope {
            variables,
            parent: None,
        })
    }

    fn define(&mut self, id: Spur, var: VariableKind, loc: Location) -> SemanticResult<()> {
        let prev = self.variables.insert(id, Variable::Source(var.at(&loc)));
        if prev.is_some() {
            Err(SemanticError::ReboundVariable.at(&loc))
        } else {
            Ok(())
        }
    }

    fn define_type(&mut self, id: Spur, t: typecheck::Type, loc: Location) -> SemanticResult<()> {
        self.define(id, VariableKind::Type(t), loc)
    }

    fn define_func(&mut self, id: Spur, t: typecheck::Type, loc: Location) -> SemanticResult<()> {
        self.define(id, VariableKind::Function(t), loc)
    }

    fn define_value(&mut self, id: Spur, t: typecheck::Type, loc: Location) -> SemanticResult<()> {
        self.define(id, VariableKind::Local(t), loc)
    }

    fn define_global(
        &mut self,
        id: Spur,
        mutable: bool,
        ty: types::LocalType,
        loc: Location,
    ) -> SemanticResult<()> {
        self.define(id, VariableKind::Global { ty, mutable }, loc)
    }

    pub fn populate_from_module(
        &mut self,
        interner: &Interner,
        module: &ast::Module,
    ) -> SemanticResult<()> {
        self.populate_types(interner, module)?;
        self.populate_items(interner, module)?;
        Ok(())
    }

    fn populate_types(&mut self, interner: &Interner, module: &ast::Module) -> SemanticResult<()> {
        let mut typegraph = DiGraphMap::<Spur, Location>::new();
        // let mut typedefs = Vec::new();
        for (_name, item) in module.named_items.iter() {
            if let ast::NamedModuleItemKind::TypeAlias(ast::TypeAliasDefinition { name, value }) =
                &item.data
            {
                match &value.data {
                    ast::TypeKind::Variable(dep) => {
                        typegraph.add_edge(*dep, *name, value.location);
                    }
                    ast::TypeKind::Function {
                        param: _,
                        result: _,
                    } => {
                        // TODO: fix this
                    }
                    ast::TypeKind::Forall { vars: _, ty: _ } => {
                        // TODO: fix this
                    }
                }
            }
        }

        let typedef_order = match petgraph::algo::toposort(&typegraph, None) {
            Ok(order) => order,
            Err(cycle) => {
                return Err(
                    SemanticError::CycleInTypeAlias.at(&module.named_items[&cycle.node_id()])
                )
            }
        };

        for id in &typedef_order {
            if let Some(_t) = self.lookup_type(*id) {
                // We don't need to do anything if the type is already defined
            } else if let Some(item) = module.named_items.get(id) {
                let type_alias_def = match &item.data {
                    ast::NamedModuleItemKind::TypeAlias(t) => t,
                    _ => panic!("non-type alias found"),
                };

                let mut typechecker =
                    Typechecker::new(Rc::new(self.clone()), interner.clone(), item.location);

                let type_alias_value = typechecker.resolve_type(&type_alias_def.value)?;

                self.define_type(*id, type_alias_value, item.location)?;
            } else {
                let (_, _, loc) = typegraph.edges(*id).next().unwrap();
                return Err(SemanticError::UnboundVariable.at(loc));
            }
        }

        Ok(())
    }

    fn populate_items(&mut self, interner: &Interner, module: &ast::Module) -> SemanticResult<()> {
        for (name, item) in module.named_items.iter() {
            let item_loc = item.location;

            let mut typechecker =
                typecheck::Typechecker::new(Rc::new(self.clone()), interner.clone(), item_loc);

            match &item.data {
                ast::NamedModuleItemKind::Function(func_def) => {
                    let func_type = typechecker.resolve_type(&func_def.func_type)?;
                    self.define_func(*name, func_type, item.location)?;
                }
                ast::NamedModuleItemKind::Constant(const_def) => {
                    let const_type = typechecker.resolve_type(&const_def.const_type)?.clone();
                    self.define_global(
                        *name,
                        false,
                        const_type
                            .to_monotype()
                            .ok_or(SemanticError::NotMonotype.at(&item_loc))?,
                        item.location,
                    )?;
                }
                ast::NamedModuleItemKind::Global(global_def) => {
                    let global_type = typechecker.resolve_type(&global_def.global_type)?.clone();
                    self.define_global(
                        *name,
                        true,
                        global_type
                            .to_monotype()
                            .ok_or(SemanticError::NotMonotype.at(&item_loc))?,
                        item.location,
                    )?;
                }
                ast::NamedModuleItemKind::TypeAlias(_) => {
                    // We already handle the type aliases in the previous pass
                }
            }
        }

        Ok(())
    }

    // pub fn populate_from_function(&mut self, func: &ast::FunctionDefinition) -> SemanticResult<()> {
    //     if

    //     for (param_name, param_type) in func.params.iter() {
    //         // If we are inside the scope of this function's module, then we know the parameter types already resolve.
    //         let param_type = self.resolve_type(param_type).unwrap().clone();
    //         self.define_value(param_name.data, param_type, param_name.location)?;
    //     }

    //     Ok(())
    // }
}

#[derive(Clone, Debug, PartialEq)]
pub enum VariableLoad<T: ExprType> {
    /// `func_type` should always be a `FunctoinReferenceType`;
    Function {
        id: Spur,
        // Should always be a FunctionReference
        func_type: T::Single,
    },
    Local {
        id: Spur,
        var_type: T::Single,
    },
    Global {
        id: Spur,
        global_type: T::Single,
    },
}

impl<T: ExprType> VariableLoad<T> {
    fn as_single_type(&self) -> T::Single {
        match self {
            VariableLoad::Function { func_type, .. } => func_type.clone(),
            VariableLoad::Local { var_type, .. } => var_type.clone(),
            VariableLoad::Global {
                global_type: const_type,
                ..
            } => const_type.clone(),
        }
    }

    fn as_multi_type(&self) -> T {
        self.as_single_type().as_multi_type()
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum VariableStore<T: ExprType> {
    Local { id: Spur, value: Box<TypedExpr<T>> },
    Global { id: Spur, value: Box<TypedExpr<T>> },
}

#[derive(Clone, Debug, PartialEq)]
pub struct InlineAsmCall<T: ExprType> {
    pub local_types: Vec<(Spur, T::Single)>,
    pub asm: Vec<sexpr::Syntax>,

    pub params: Vec<TypedExpr<T>>,
    pub result: T,
}

// #[derive(Clone, Debug, PartialEq)]
// pub struct LetBinding<T: ExprType> {
// }

#[derive(Clone, Debug, PartialEq)]
pub enum TypedExprKind<T: ExprType> {
    FunctionCall {
        func: Box<TypedExpr<T>>,
        params: Vec<TypedExpr<T>>,
        result: T,
    },
    InlineAsmCall(InlineAsmCall<T>),
    IntLiteral {
        int: num::BigInt,
        int_type: T::Single,
    },
    FloatLiteral {
        float: f64,
        float_type: T::Single,
    },
    VariableLoad(VariableLoad<T>),
    VariableStore(VariableStore<T>),
    // LetBinding(),
}

impl<T: ExprType> TypedExprKind<T> {
    pub fn as_single_type(&self) -> Option<T::Single> {
        match self {
            // TypedExprKind::DirectFunctionCall { result, .. } => result.as_single(),
            TypedExprKind::FunctionCall { result, .. } => result.as_single_type(),
            TypedExprKind::InlineAsmCall(InlineAsmCall { result, .. }) => result.as_single_type(),
            TypedExprKind::IntLiteral { int_type, .. } => Some(int_type.clone()),
            TypedExprKind::FloatLiteral { float_type, .. } => Some(float_type.clone()),
            TypedExprKind::VariableLoad(vl) => Some(vl.as_single_type()),
            TypedExprKind::VariableStore(_) => None,
        }
    }

    pub fn as_multi_type(&self) -> T {
        match self {
            // TypedExprKind::DirectFunctionCall { result, .. } => result.clone(),
            TypedExprKind::FunctionCall { result, .. } => result.clone(),
            TypedExprKind::InlineAsmCall(InlineAsmCall { result, .. }) => result.clone(),
            TypedExprKind::IntLiteral { int_type, .. } => int_type.clone().as_multi_type(),
            TypedExprKind::FloatLiteral { float_type, .. } => float_type.clone().as_multi_type(),
            TypedExprKind::VariableLoad(vl) => vl.as_multi_type(),
            TypedExprKind::VariableStore(_) => T::unit_type(),
        }
    }

    fn as_type_slice_mut(&mut self) -> &mut [T::Single] {
        match self {
            // TypedExprKind::DirectFunctionCall { result, .. } => result.values.as_mut(),
            TypedExprKind::FunctionCall { result, .. } => result.as_mut_slice(),
            TypedExprKind::InlineAsmCall(InlineAsmCall { result, .. }) => result.as_mut_slice(),
            TypedExprKind::IntLiteral { int_type, .. } => std::slice::from_mut(int_type),
            TypedExprKind::FloatLiteral { float_type, .. } => std::slice::from_mut(float_type),
            TypedExprKind::VariableLoad(vl) => match vl {
                VariableLoad::Function { func_type, .. } => std::slice::from_mut(func_type),
                VariableLoad::Global {
                    global_type: const_type,
                    ..
                } => std::slice::from_mut(const_type),
                VariableLoad::Local { var_type, .. } => std::slice::from_mut(var_type),
            },
            TypedExprKind::VariableStore(_) => &mut [],
        }
    }
}

pub type TypedExpr<T = types::Values> = Locatable<TypedExprKind<T>>;

#[derive(Clone, Debug)]
pub struct FunctionDefinition {
    pub name: Spur,
    pub internal_scope: Rc<VariableScope>,
    pub argument_ids: Vec<Spur>,
    pub body: TypedExpr<types::Values>,
    pub type_signature: types::FunctionReferenceType,
}

impl FunctionDefinition {
    fn from_hir(
        scope: &Rc<VariableScope>,
        interner: &Interner,
        hir_func: Locatable<&ast::FunctionDefinition>,
    ) -> SemanticResult<FunctionDefinition> {
        let internal_scope = Rc::new({
            // function_scope
            //     .populate_from_function(hir_func.data)
            //     .unwrap();
            Scope::<Variable>::new_from(scope)
        });

        let mut typechecker = typecheck::Typechecker::new(
            internal_scope.clone(),
            interner.clone(),
            hir_func.location,
        );

        let (param_type, body) = typechecker.check_function(&hir_func)?;

        let result_type = body
            .data
            .as_multi_type()
            .to_monotype()
            .ok_or(SemanticError::NotMonotype.at(&hir_func))?;

        Ok(FunctionDefinition {
            name: hir_func.name,
            internal_scope,
            argument_ids: hir_func.params.iter().map(|id| id.data).collect(),
            body: body.to_concrete()?,
            type_signature: types::FunctionReferenceType {
                param: param_type
                    .to_monotype()
                    .ok_or(SemanticError::NotMonotype.at(&hir_func))?,
                result: result_type,
            },
        })
    }
}

#[derive(Clone, Debug)]
pub struct GlobalDefinition {
    pub name: Spur,
    pub mutable: bool,
    pub global_type: types::LocalType,
    pub body: TypedExpr,
    pub internal_scope: Rc<VariableScope>,
}

impl GlobalDefinition {
    fn from_hir_const(
        scope: &Rc<VariableScope>,
        interner: &Interner,
        hir: Locatable<&ast::ConstantDefinition>,
    ) -> SemanticResult<GlobalDefinition> {
        let mut typechecker =
            typecheck::Typechecker::new(scope.clone(), interner.clone(), hir.location);

        let const_type = typechecker.resolve_type(&hir.const_type)?.clone();

        Ok(GlobalDefinition {
            name: hir.name,
            mutable: false,
            body: typechecker
                .check(
                    &hir.data.value,
                    &typecheck::Values::from_single(const_type.clone()),
                )?
                .to_concrete()?,
            global_type: const_type
                .to_monotype()
                .ok_or(SemanticError::NotMonotype.at(&hir))?,
            internal_scope: scope.clone(),
        })
    }

    fn from_hir_global(
        scope: &Rc<VariableScope>,
        interner: &Interner,
        hir: Locatable<&ast::GlobalDefinition>,
    ) -> SemanticResult<GlobalDefinition> {
        let mut typechecker =
            typecheck::Typechecker::new(scope.clone(), interner.clone(), hir.location);

        let const_type = typechecker.resolve_type(&hir.global_type)?.clone();

        Ok(GlobalDefinition {
            name: hir.name,
            mutable: true,
            body: typechecker
                .check(
                    &hir.data.value,
                    &typecheck::Values::from_single(const_type.clone()),
                )?
                .to_concrete()?,
            global_type: const_type
                .to_monotype()
                .ok_or(SemanticError::NotMonotype.at(&hir))?,
            internal_scope: scope.clone(),
        })
    }
}

#[derive(Clone, Debug)]
pub enum ModuleItemKind {
    Function(FunctionDefinition),
    Global(GlobalDefinition),
}

impl ModuleItemKind {
    fn name(&self) -> Spur {
        match self {
            ModuleItemKind::Global(GlobalDefinition { name, .. })
            | ModuleItemKind::Function(FunctionDefinition { name, .. }) => *name,
        }
    }
}

pub type ModuleItem = Locatable<ModuleItemKind>;

impl ModuleItem {
    fn from_hir(
        scope: &Rc<VariableScope>,
        interner: &Interner,
        hir: &ast::NamedModuleItem,
    ) -> SemanticResult<Option<ModuleItem>> {
        Ok(match &hir.data {
            ast::NamedModuleItemKind::TypeAlias(_) => {
                // Type aliases are used in the MIR phase, but do not pass it.
                None
            }
            ast::NamedModuleItemKind::Function(f) => Some(
                ModuleItemKind::Function(FunctionDefinition::from_hir(scope, interner, f.at(hir))?)
                    .at(hir),
            ),
            ast::NamedModuleItemKind::Constant(c) => Some(
                ModuleItemKind::Global(GlobalDefinition::from_hir_const(
                    scope,
                    interner,
                    c.at(hir),
                )?)
                .at(hir),
            ),
            ast::NamedModuleItemKind::Global(g) => Some(
                ModuleItemKind::Global(GlobalDefinition::from_hir_global(
                    scope,
                    interner,
                    g.at(hir),
                )?)
                .at(hir),
            ),
        })
    }
}

#[derive(Clone, Debug)]
pub struct Module {
    pub items: HashMap<Spur, ModuleItem>,
    pub exports: Vec<(Spur, Spur)>,
}

impl Module {
    pub fn from_hir(
        builtins: &Rc<VariableScope>,
        interner: &Interner,
        hir_module: &ast::Module,
    ) -> SemanticResult<Module> {
        let module_type_scope = Rc::new({
            let mut module_type_scope = Scope::new_from(builtins);
            module_type_scope.populate_from_module(interner, hir_module)?;
            module_type_scope
        });

        let items = hir_module
            .named_items
            .iter()
            .filter_map(|(_, item)| {
                ModuleItem::from_hir(&module_type_scope, interner, item).transpose()
            })
            .map(|r| r.map(|item| (item.name(), item)))
            .collect::<SemanticResult<HashMap<Spur, ModuleItem>>>()?;

        let exports = hir_module
            .exports
            .iter()
            .map(|e| (e.export_name, e.func))
            .collect();

        Ok(Module { items, exports })
    }
}

pub trait SingleExprType: Clone + std::fmt::Debug + PartialEq {
    type Multi: ExprType<Single = Self>;

    fn as_multi_type(&self) -> Self::Multi {
        Self::Multi::from(self.clone())
    }

    fn as_slice(&self) -> &[Self] {
        std::slice::from_ref(self)
    }

    fn as_mut_slice(&mut self) -> &mut [Self] {
        std::slice::from_mut(self)
    }
}

pub trait ExprType:
    Clone
    + std::fmt::Debug
    + AsMut<[Self::Single]>
    + AsRef<[Self::Single]>
    + From<Self::Single>
    + FromIterator<Self::Single>
    + PartialEq
{
    type Single: SingleExprType<Multi = Self>;

    fn as_single_type(&self) -> Option<Self::Single> {
        match self.as_ref() {
            [t] => Some(t.clone()),
            _ => None,
        }
    }

    fn as_slice(&self) -> &[Self::Single] {
        self.as_ref()
    }

    fn as_mut_slice(&mut self) -> &mut [Self::Single] {
        self.as_mut()
    }

    fn unit_type() -> Self {
        std::iter::empty().collect()
    }
}

impl SingleExprType for types::LocalType {
    type Multi = types::Values;
}

impl ExprType for types::Values {
    type Single = types::LocalType;
}
