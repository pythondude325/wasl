use super::interner::Interner;
use crate::common::*;
use crate::sexpr::{SExpr, Syntax};
use lasso::Spur;
use std::collections::HashMap;
use thiserror::Error;

pub mod expr;
pub use expr::{Expr, ExprType};
pub mod types;
pub use types::{Type, TypeKind};

pub const PREINTERN_SYMBOLS: &[&str] = &[
    expr::INLINE_ASM_SYMBOL,
    expr::TYPE_ANNOTATION_SYMBOL,
    expr::SET_SYMBOL,
    types::FUNC_TYPE_SYMBOL,
    types::FUNC_TYPE_ALT_SYMBOL,
    types::FORALL_TYPE_SYMBOL,
    types::FORALL_TYPE_ALT_SYMBOL,
    FUNCTION_DEFINITION_SYMBOL,
    TYPE_ALIAS_DEFINITION_SYMBOL,
    CONST_DEFINITION_SYMBOL,
    GLOBAL_DEFINITION_SYMBOL,
    EXPORT_DEFINITION_SYMBOL,
];

/// An error that may occur while parsing HIR
#[derive(Clone, Error, Debug)]
pub enum SyntaxError {
    /// Invalid type syntax
    #[error("Invalid type syntax")]
    InvalidType,

    /// Invalid inline assembly syntax
    #[error("Invalid inline assembly syntax")]
    InvalidInlineAsmSyntax,

    /// Invalid type annotation syntax
    #[error("Invalid type annotation syntax")]
    InvalidTypeAnnotationSyntax,

    /// Invalid expression syntax
    #[error("Invalid expression syntax")]
    InvalidExprSyntax,

    /// Invalid module item syntax
    #[error("Invalid module item syntax")]
    InvalidModuleItemSyntax,

    /// Invalid constant definition syntax
    #[error("Invalid constant definition syntax")]
    InvalidConstantDefinitionSyntax,

    /// Invalid global definition syntax
    #[error("Invalid global definition syntax")]
    InvalidGlobalDefinitionSyntax,

    /// Invalid type alias syntax
    #[error("Invalid type alias syntax")]
    InvalidTypeAliasSyntax,

    /// Invlid function definition syntax
    #[error("Invalid function definition syntax")]
    InvalidFunctionDefinition,

    #[error("Invalid export syntax")]
    InvalidExport,

    #[error("Invalid variable set syntax")]
    InvalidVariableSet,

    #[error("Invalid syntax in for type")]
    InvalidForall,
}

type SyntaxResult<T> = LResult<T, SyntaxError>;

// Shared context used by most parser functions
#[derive(Debug)]
pub struct ParserContext {
    pub interner: Interner,
    // pub scope: &'s Scope,
}

#[derive(Clone, Debug)]
pub struct FunctionDefinition {
    pub name: Spur,
    pub params: Vec<Locatable<Spur>>,
    pub func_type: Type,
    pub body: Expr,
}

const FUNCTION_DEFINITION_SYMBOL: &str = "def-func";

impl FunctionDefinition {
    pub fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<FunctionDefinition> {
        let function_definition_symbool = ctx
            .interner
            .borrow()
            .get(FUNCTION_DEFINITION_SYMBOL)
            .unwrap();

        match source.as_list() {
            Some(&[syntax_pat!(SExpr::Symbol(id)), ref param_list, ref func_type, ref body])
                if id == function_definition_symbool =>
            {
                let (name, params) = match param_list.as_list() {
                    Some(&[syntax_pat!(SExpr::Symbol(func_name)), ref params @ ..]) => (
                        func_name,
                        params
                            .iter()
                            .map(|p| match p {
                                syntax_pat!(SExpr::Symbol(param_name)) => Ok((*param_name).at(p)),
                                _ => Err(SyntaxError::InvalidFunctionDefinition.at(p)),
                            })
                            .collect::<SyntaxResult<Vec<_>>>()?,
                    ),
                    _ => {
                        return Err(SyntaxError::InvalidFunctionDefinition.at(param_list));
                    }
                };

                let func_type = Type::parse(func_type, ctx)?;

                let body = Expr::parse(body, ctx)?;

                Ok(FunctionDefinition {
                    name,
                    params,
                    func_type,
                    body,
                })
            }
            _ => Err(SyntaxError::InvalidFunctionDefinition.at(source)),
        }
    }
}

#[derive(Clone, Debug)]
pub struct TypeAliasDefinition {
    pub name: Spur,
    pub value: Type,
}

const TYPE_ALIAS_DEFINITION_SYMBOL: &str = "def-type";

impl TypeAliasDefinition {
    // fn parse(&source, )
    pub fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<TypeAliasDefinition> {
        let type_alias_definition_symbol = ctx
            .interner
            .borrow()
            .get(TYPE_ALIAS_DEFINITION_SYMBOL)
            .unwrap();

        match source.as_list() {
            Some(
                &[syntax_pat!(SExpr::Symbol(id)), syntax_pat!(SExpr::Symbol(name)), ref type_source],
            ) if id == type_alias_definition_symbol => {
                let value = Type::parse(type_source, ctx)?;
                Ok(TypeAliasDefinition { name, value })
            }
            _ => Err(SyntaxError::InvalidTypeAliasSyntax.at(source)),
        }
    }
}

#[derive(Clone, Debug)]
pub struct ConstantDefinition {
    pub name: Spur,
    pub const_type: Type,
    pub value: Expr,
}

const CONST_DEFINITION_SYMBOL: &str = "def-const";

impl ConstantDefinition {
    pub fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<ConstantDefinition> {
        let const_definition_symbol = ctx.interner.borrow().get(CONST_DEFINITION_SYMBOL).unwrap();

        match source.as_list() {
            Some(
                &[syntax_pat!(SExpr::Symbol(id)), syntax_pat!(SExpr::Symbol(name)), ref type_source, ref expression_source],
            ) if id == const_definition_symbol => {
                let const_type = Type::parse(type_source, ctx)?;
                let value = Expr::parse(expression_source, ctx)?;
                Ok(ConstantDefinition {
                    name,
                    const_type,
                    value,
                })
            }
            _ => Err(SyntaxError::InvalidConstantDefinitionSyntax.at(source)),
        }
    }
}

#[derive(Clone, Debug)]
pub struct GlobalDefinition {
    pub name: Spur,
    pub global_type: Type,
    pub value: Expr,
}

const GLOBAL_DEFINITION_SYMBOL: &str = "def-global";

impl GlobalDefinition {
    pub fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<GlobalDefinition> {
        let global_definition_symbol = ctx.interner.borrow().get(GLOBAL_DEFINITION_SYMBOL).unwrap();

        match source.as_list() {
            Some(
                &[syntax_pat!(SExpr::Symbol(id)), syntax_pat!(SExpr::Symbol(name)), ref type_source, ref expression_source],
            ) if id == global_definition_symbol => {
                let global_type = Type::parse(type_source, ctx)?;
                let value = Expr::parse(expression_source, ctx)?;
                Ok(GlobalDefinition {
                    name,
                    global_type,
                    value,
                })
            }
            _ => Err(SyntaxError::InvalidGlobalDefinitionSyntax.at(source)),
        }
    }
}

#[derive(Clone, Debug)]
pub struct ExportDefinition {
    pub func: Spur,
    pub export_name: Spur,
}

const EXPORT_DEFINITION_SYMBOL: &str = "export";

impl ExportDefinition {
    pub fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<ExportDefinition> {
        let func_export_symbol = ctx.interner.borrow().get(EXPORT_DEFINITION_SYMBOL).unwrap();

        match source.as_list() {
            Some(&[syntax_pat!(SExpr::Symbol(id)), syntax_pat!(SExpr::Symbol(name))])
                if id == func_export_symbol =>
            {
                Ok(ExportDefinition {
                    func: name,
                    export_name: name,
                })
            }
            _ => Err(SyntaxError::InvalidExport.at(source)),
        }
    }
}

#[derive(Clone, Debug)]
pub enum NamedModuleItemKind {
    Function(FunctionDefinition),
    TypeAlias(TypeAliasDefinition),
    Constant(ConstantDefinition),
    Global(GlobalDefinition),
}

impl NamedModuleItemKind {
    fn name(&self) -> Spur {
        match self {
            NamedModuleItemKind::Function(FunctionDefinition { name, .. })
            | NamedModuleItemKind::TypeAlias(TypeAliasDefinition { name, .. })
            | NamedModuleItemKind::Constant(ConstantDefinition { name, .. })
            | NamedModuleItemKind::Global(GlobalDefinition { name, .. }) => *name,
        }
    }
}

pub type NamedModuleItem = Locatable<NamedModuleItemKind>;

#[derive(Clone, Debug)]
pub enum ModuleItem {
    Named(NamedModuleItem),
    Export(Locatable<ExportDefinition>),
}

impl ModuleItem {
    fn parse(source: &Syntax, ctx: &ParserContext) -> SyntaxResult<ModuleItem> {
        let function_definition_symbol = ctx
            .interner
            .borrow()
            .get(FUNCTION_DEFINITION_SYMBOL)
            .unwrap();
        let type_alias_definition_symbol = ctx
            .interner
            .borrow()
            .get(TYPE_ALIAS_DEFINITION_SYMBOL)
            .unwrap();
        let const_definition_symbol = ctx.interner.borrow().get(CONST_DEFINITION_SYMBOL).unwrap();
        let global_definition_symbol = ctx.interner.borrow().get(GLOBAL_DEFINITION_SYMBOL).unwrap();
        let export_symbol = ctx.interner.borrow().get(EXPORT_DEFINITION_SYMBOL).unwrap();

        Ok(match source.as_list() {
            Some(&[syntax_pat!(SExpr::Symbol(id)), ..]) => {
                if id == function_definition_symbol {
                    ModuleItem::Named(
                        NamedModuleItemKind::Function(FunctionDefinition::parse(source, ctx)?)
                            .at(source),
                    )
                } else if id == type_alias_definition_symbol {
                    ModuleItem::Named(
                        NamedModuleItemKind::TypeAlias(TypeAliasDefinition::parse(source, ctx)?)
                            .at(source),
                    )
                } else if id == const_definition_symbol {
                    ModuleItem::Named(
                        NamedModuleItemKind::Constant(ConstantDefinition::parse(source, ctx)?)
                            .at(source),
                    )
                } else if id == global_definition_symbol {
                    ModuleItem::Named(
                        NamedModuleItemKind::Global(GlobalDefinition::parse(source, ctx)?)
                            .at(source),
                    )
                } else if id == export_symbol {
                    ModuleItem::Export(ExportDefinition::parse(source, ctx)?.at(source))
                } else {
                    return Err(SyntaxError::InvalidModuleItemSyntax.at(source));
                }
            }
            _ => return Err(SyntaxError::InvalidModuleItemSyntax.at(source)),
        })
    }
}

#[derive(Clone, Debug)]
pub struct Module {
    pub named_items: HashMap<Spur, NamedModuleItem>,
    pub exports: Vec<Locatable<ExportDefinition>>,
}

impl Module {
    pub fn parse(source: &[Syntax], ctx: &ParserContext) -> SyntaxResult<Module> {
        let mut named_items = HashMap::new();
        let mut exports = Vec::new();

        for e in source {
            match ModuleItem::parse(e, ctx)? {
                ModuleItem::Named(n) => {
                    named_items.insert(n.name(), n);
                }
                ModuleItem::Export(e) => {
                    exports.push(e);
                }
            }
        }
        Ok(Module {
            named_items,
            exports,
        })
    }
}

// pub fn parse_module(source: &[Syntax]) -> Module

// #[derive(Clone, Debug)]
// pub enum ModuleItemType {
//     Function(types::FunctionReferenceType),
//     Type,
//     Constant(types::LocalType),
// }
