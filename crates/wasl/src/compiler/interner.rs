use lasso::{Rodeo, Spur};
use std::cell::{Ref, RefCell, RefMut};
use std::rc::Rc;

// TODO: maybe make this a Arc<RwLock<Rodeo>> later if i go multi-threaded

#[derive(Clone, Debug, PartialEq)]
pub struct Interner {
    interner: Rc<RefCell<Rodeo>>,
}

impl Default for Interner {
    fn default() -> Self {
        Self::new()
    }
}

impl Interner {
    pub fn new() -> Interner {
        let mut interner = Rodeo::new();

        for &sym in crate::types::primitive::PREINTERN_SYMBOLS {
            interner.get_or_intern_static(sym);
        }
        for &sym in super::ast::PREINTERN_SYMBOLS {
            interner.get_or_intern_static(sym);
        }
        for &sym in super::lir::PREINTERN_SYMBOLS {
            interner.get_or_intern_static(sym);
        }

        Interner {
            interner: Rc::new(RefCell::new(interner)),
        }
    }

    pub fn static_symbol(&self, s: &'static str) -> Spur {
        self.borrow().get(s).unwrap()
    }

    pub fn borrow(&self) -> Ref<'_, Rodeo> {
        self.interner.borrow()
    }

    pub fn borrow_mut(&self) -> RefMut<'_, Rodeo> {
        self.interner.borrow_mut()
    }

    pub fn generate_symbol(&self, prefix: &str) -> Spur {
        let mut interner = self.borrow_mut();

        let n = interner.len() + 1;

        let first_try_name = format!("{}{}", prefix, n);

        if interner.contains(&first_try_name) {
            let mut m = 1;
            let name = loop {
                let name = format!("{}{}-{}", prefix, n, m);
                if !interner.contains(&name) {
                    break name;
                }
                m += 1;
            };
            interner.get_or_intern(name)
        } else {
            interner.get_or_intern(first_try_name)
        }
    }
}
