program = { SOI ~ datum* ~ EOI }

symbol = @{
	( '0'..'9'
    | 'a'..'z'
    | 'A'..'Z'
    | "!" | "#" | "$" | "%" | "&" | "'" | "*" | "+" | "-" | "." | "/"
    | ":" | "<" | "=" | ">" | "?" | "@" | "\\" | "^" | "_" | "`" | "|" | "~"
    )+
}

datum = { list | string | float | integer | symbol }

string = ${
	"\"" ~ (string_char | string_char_unicode | string_byte)* ~ "\""
}
string_byte = { "\\" ~ ASCII_HEX_DIGIT ~ ASCII_HEX_DIGIT }
string_char_unicode = {
    "\\u{" ~ hex_num ~ "}"
}
string_char = {
	!("\"" | "\\") ~ ANY
    | "\\t"
    | "\\n"
    | "\\r"
    | "\\\""
    | "\\'"
    | "\\\\"
}

list = { "(" ~ datum* ~ ")" }

sign = { "+" | "-" }
dec_num = { ASCII_DIGIT ~ ("_"? ~ ASCII_DIGIT)* }
hex_num = { ASCII_HEX_DIGIT ~ ("_"? ~ ASCII_HEX_DIGIT)* }

integer = @{ dec_integer | hex_integer }
dec_integer = { sign? ~ dec_num }
hex_integer = { sign? ~ ^"0x" ~ hex_num }

float = @{
	sign? ~ (
    	dec_float
        | hex_float
        | "inf"
        | "nan"
    )
}
dec_float = {
    dec_num ~ "." ~ dec_num
	| dec_num ~ "."
    | dec_num ~ ("." ~ dec_num?)? ~ ^"e" ~ sign? ~ dec_num
}
hex_float = {
    "0x" ~ hex_num ~ "." ~ hex_num
	| "0x" ~ hex_num ~ "."
    | "0x" ~ hex_num ~ ("." ~ hex_num?)? ~ ^"p" ~ sign? ~ dec_num
}




WHITESPACE = _{ " " | "\t" | NEWLINE }

line_comment = @{ ";;" ~ (!NEWLINE ~ ANY)* ~ (NEWLINE | &EOI) }
block_comment = @{ "(;" ~ block_comment_char* ~ ";)" }
block_comment_char = {
	(";" ~ !")")
    | ("(" ~ !";")
	| !(";"|"(") ~ ANY
    | block_comment
}
COMMENT = _{ line_comment | block_comment }