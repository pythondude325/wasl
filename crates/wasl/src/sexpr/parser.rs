use crate::common::*;
use crate::compiler::interner::Interner;
use num::BigInt;
use pest::Parser;
use pest_derive::Parser;
use thiserror::Error;

use super::{SExpr, Syntax};

#[derive(Parser)]
#[grammar = "sexpr/grammar.pest"]
struct SExprParser;

#[derive(Debug, Error)]
pub enum SExprParseError {
    #[error("Error in parsing: {source}")]
    PestError {
        #[from]
        source: pest::error::Error<Rule>,
    },

    #[error("Invalid unicode escape: {escape}")]
    InvalidUnicodeEscape { escape: String },

    #[error("Error parsing float: {source}")]
    FloatParseError {
        #[from]
        source: std::num::ParseFloatError,
    },

    #[error("Error parsing integer: {source}")]
    IntParseError {
        #[from]
        source: num::bigint::ParseBigIntError,
    },
}

pub fn parse(source: &str, interner: &Interner) -> LResult<Vec<Syntax>, SExprParseError> {
    let mut result = SExprParser::parse(Rule::program, source).unwrap();

    let program_pair = result.next().unwrap();
    assert!(result.next().is_none());

    let mut program = Vec::new();
    for pair in program_pair.into_inner() {
        if pair.as_rule() != Rule::EOI {
            program.push(parse_datum(pair, interner)?);
        }
    }

    Ok(program)
}

fn parse_datum(
    pair: pest::iterators::Pair<'_, Rule>,
    interner: &Interner,
) -> LResult<Syntax, SExprParseError> {
    match pair.as_rule() {
        Rule::datum => {
            let span = Location::from(pair.as_span());

            let mut inner = pair.into_inner();

            let expr = inner.next().unwrap();
            assert!(inner.next().is_none());

            match expr.as_rule() {
                Rule::symbol => {
                    let symbol_id = interner.borrow_mut().get_or_intern(expr.as_str());
                    Ok(Syntax::Parsed(SExpr::Symbol(symbol_id).at(&span)))
                }
                Rule::string => {
                    let mut buf = String::new();
                    for string_elem_pair in expr.into_inner() {
                        match string_elem_pair.as_rule() {
                            Rule::string_char => buf.push(match string_elem_pair.as_str() {
                                "\\t" => '\t',
                                "\\n" => '\n',
                                "\\r" => '\r',
                                "\\\"" => '\"',
                                "\\\'" => '\'',
                                "\\\\" => '\\',
                                _ => string_elem_pair.as_str().chars().next().unwrap(),
                            }),
                            Rule::string_char_unicode => {
                                let src = string_elem_pair.as_str();
                                let num = u32::from_str_radix(&src[3..src.len() - 1], 16)
                                    .map_err(|_e| SExprParseError::InvalidUnicodeEscape {
                                        escape: string_elem_pair.as_str().to_owned(),
                                    })
                                    .map_err(span.wrapper())?;
                                let c = char::from_u32(num)
                                    .ok_or_else(|| SExprParseError::InvalidUnicodeEscape {
                                        escape: string_elem_pair.as_str().to_owned(),
                                    })
                                    .map_err(span.wrapper())?;
                                buf.push(c);
                            }
                            Rule::string_byte => {
                                unimplemented!("String bytes are currently not supported.");
                            }
                            _ => panic!("Unexpected pair"),
                        }
                    }

                    Ok(Syntax::Parsed(SExpr::String(buf).at(&span)))
                }
                Rule::integer => Ok(Syntax::Parsed(
                    SExpr::Integer(
                        expr.as_str()
                            .parse::<BigInt>()
                            .map_err(|e| SExprParseError::from(e).at(&span))?,
                    )
                    .at(&span),
                )),
                Rule::float => {
                    let f = expr
                        .as_str()
                        .parse::<f64>()
                        .map_err(|e| SExprParseError::from(e).at(&span))?;
                    Ok(Syntax::Parsed(SExpr::Float(f).at(&span)))
                }
                Rule::list => Ok(Syntax::Parsed(
                    expr.into_inner()
                        .map(|e| parse_datum(e, interner))
                        .collect::<LResult<SExpr, SExprParseError>>()?
                        .at(&span),
                )),
                _ => {
                    panic!("invalid rule")
                }
            }
        }
        _ => {
            dbg!(pair);
            panic!("unexpected thing")
        }
    }
}
