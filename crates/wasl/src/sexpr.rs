use crate::common::*;
use crate::compiler::interner::Interner;
use lasso::Spur;
use num::BigInt;
use std::io::{self, Write};
use std::iter;

pub mod parser;
pub use parser::*;

macro_rules! syntax_pat {
    ($p:pat) => {
        Syntax::Parsed(Locatable { data: $p, .. }) | Syntax::Generated($p)
    };
}

#[derive(Clone, Debug, PartialEq)]
pub enum SExpr {
    Symbol(Spur),
    String(String),
    Integer(BigInt),
    Float(f64),
    List(Vec<Syntax>),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Syntax {
    Parsed(Locatable<SExpr>),
    Generated(SExpr),
}

pub struct SyntaxListBuilder {
    s: Vec<Syntax>,
}

impl SyntaxListBuilder {
    pub fn sym(self, id: Spur) -> SyntaxListBuilder {
        self.syntax(Syntax::build_id(id))
    }

    pub fn int(self, n: impl Into<num::BigInt>) -> SyntaxListBuilder {
        self.syntax(Syntax::Generated(SExpr::Integer(n.into())))
    }

    pub fn float(self, f: impl Into<f64>) -> SyntaxListBuilder {
        self.syntax(Syntax::Generated(SExpr::Float(f.into())))
    }

    pub fn string(self, s: impl AsRef<str>) -> SyntaxListBuilder {
        self.syntax(Syntax::Generated(SExpr::String(s.as_ref().to_owned())))
    }

    pub fn syntax(mut self, s: Syntax) -> SyntaxListBuilder {
        self.s.push(s);
        self
    }

    pub fn append(mut self, iter: impl IntoIterator<Item = Syntax>) -> SyntaxListBuilder {
        self.s.extend(iter.into_iter());
        self
    }

    pub fn build(self) -> Syntax {
        Syntax::Generated(SExpr::List(self.s))
    }

    pub fn build_with_location(self, location: Location) -> Syntax {
        Syntax::Parsed(SExpr::List(self.s).at(&location))
    }
}

impl Syntax {
    pub fn build_id(id: Spur) -> Syntax {
        Syntax::Generated(SExpr::Symbol(id))
    }

    pub fn build_list() -> SyntaxListBuilder {
        SyntaxListBuilder { s: Vec::new() }
    }

    pub fn sexpr(&self) -> &SExpr {
        match self {
            Syntax::Generated(sexpr) | Syntax::Parsed(Locatable { data: sexpr, .. }) => sexpr,
        }
    }

    pub fn location(&self) -> Option<Location> {
        match self {
            &Syntax::Parsed(Locatable { location, .. }) => Some(location),
            _ => None,
        }
    }
}

impl IntoIterator for Syntax {
    type Item = Syntax;

    type IntoIter = Box<dyn Iterator<Item = Syntax>>;

    fn into_iter(self) -> Self::IntoIter {
        if let Syntax::Generated(SExpr::List(v))
        | Syntax::Parsed(Locatable {
            data: SExpr::List(v),
            ..
        }) = self
        {
            Box::new(v.into_iter())
        } else {
            Box::new(iter::once(self))
        }
    }
}

impl AsLocation for Syntax {
    fn as_location(&self) -> Location {
        self.location().expect("Unable to generate location")
    }
}

impl std::ops::Deref for Syntax {
    type Target = SExpr;
    fn deref(&self) -> &Self::Target {
        self.sexpr()
    }
}

impl From<SExpr> for Syntax {
    fn from(s: SExpr) -> Self {
        Syntax::Generated(s)
    }
}

impl From<Locatable<SExpr>> for Syntax {
    fn from(s: Locatable<SExpr>) -> Self {
        Syntax::Parsed(s)
    }
}

impl From<Spur> for Syntax {
    fn from(id: Spur) -> Self {
        Syntax::Generated(SExpr::Symbol(id))
    }
}

impl FromIterator<Syntax> for SExpr {
    fn from_iter<T: IntoIterator<Item = Syntax>>(iter: T) -> Self {
        SExpr::List(iter.into_iter().collect::<Vec<Syntax>>())
    }
}

impl SExpr {
    // pub fn symbol_from_str(s: &str) -> SExpr {
    //     SExpr::Symbol(s.to_owned())
    // }

    // pub fn read(source: &str) -> Result<SExpr, SExprParseError> {
    //     let mut parse_result = SExprParser::parse(Rule::datum, source).context(PestSnafu)?;
    //     parse_datum(parse_result.next().unwrap())
    // }

    pub fn as_list(&self) -> Option<&[Syntax]> {
        if let SExpr::List(items) = self {
            Some(items.as_slice())
        } else {
            None
        }
    }

    pub fn write<W: Write>(&self, w: &mut W, interner: &Interner) -> io::Result<()> {
        match self {
            SExpr::Symbol(s) => {
                write!(w, "{}", interner.borrow().resolve(s))?;
            }
            SExpr::String(s) => {
                write!(w, "\"")?;
                for c in s.chars() {
                    match c {
                        '\t' => write!(w, "\\t"),
                        '\n' => write!(w, "\\n"),
                        '\r' => write!(w, "\\r"),
                        '\"' => write!(w, "\\\""),
                        '\'' => write!(w, "\\\'"),
                        '\\' => write!(w, "\\\\"),
                        c if c > '~' => write!(w, "\\u{{{:x}}}", c as u32),
                        c => write!(w, "{}", c),
                    }?;
                }
                write!(w, "\"")?;
            }
            SExpr::Integer(i) => {
                write!(w, "{}", i)?;
            }
            SExpr::Float(f) => write!(w, "{}", f)?,
            SExpr::List(items) => {
                write!(w, "(")?;
                if !items.is_empty() {
                    items[0].write(w, interner)?;

                    for item in &items[1..] {
                        write!(w, " ")?;
                        item.write(w, interner)?;
                    }
                }
                write!(w, ")")?;
            }
        }

        Ok(())
    }
}

// TODO: remove these
macro_rules! sexpr_impl_from_signed_int {
    ($t:ty) => {
        impl From<$t> for SExpr {
            fn from(n: $t) -> SExpr {
                SExpr::Integer(BigInt::from(n))
            }
        }
    };
}
sexpr_impl_from_signed_int!(i8);
sexpr_impl_from_signed_int!(i16);
sexpr_impl_from_signed_int!(i32);
sexpr_impl_from_signed_int!(i64);

macro_rules! sexpr_impl_from_unsigned_int {
    ($t:ty) => {
        impl From<$t> for SExpr {
            fn from(n: $t) -> SExpr {
                SExpr::Integer(BigInt::from(n))
            }
        }
    };
}
sexpr_impl_from_unsigned_int!(u8);
sexpr_impl_from_unsigned_int!(u16);
sexpr_impl_from_unsigned_int!(u32);
sexpr_impl_from_unsigned_int!(u64);

impl From<&str> for SExpr {
    fn from(s: &str) -> Self {
        SExpr::String(s.to_owned())
    }
}

impl From<Vec<Syntax>> for SExpr {
    fn from(v: Vec<Syntax>) -> Self {
        SExpr::List(v)
    }
}

impl From<num::BigInt> for SExpr {
    fn from(n: num::BigInt) -> Self {
        SExpr::Integer(n)
    }
}
